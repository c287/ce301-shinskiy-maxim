# CE301 Capstone project - Dart Throwing Robot

### Table of Content

---

* [Challenge Week](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Challenge%20Week) 
  * [Background Literature](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Challenge%20Week/Background%20Literature)
  * [Technical activity deliverable](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Challenge%20Week/Technical%20activity%20deliverable)
    * [Task](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Challenge%20Week/Technical%20activity%20deliverable/Task)

* [Project code](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code)
  * [Development](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Development)
    * [3DoF](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Development/3DoF)
    * [DartTrajectory](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Development/DartTrajectory)
    * [Interim Interview](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Development/Interim%20Interview)
    * [Jacobians](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Development/Jacobians)
    * [Machine Learning](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Development/Machine%20Learning)
      * [Code](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Development/Machine%20Learning/Code)
      * [Debug](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Development/Machine%20Learning/Debug)
      * [Testing](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Development/Machine%20Learning/Testing)
  * [Literature](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Literature)
  * [Product](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Product)
    * [Genetic Algorithm](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Product/Genetic%20Algorithm)
      * [Tests](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Product/Genetic%20Algorithm/Tests)
    * [Interim Interview](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Product/Interim%20Interview)
    * [Open Day](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Product/Open%20Day)

* [doc src](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/doc%20src)

---

This is a repository of a Dart Throwing Robot project, it contains all files related to it. Above table of content shows the structure of this repository.
