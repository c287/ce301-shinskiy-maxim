function [] = visualize(traj, index, angleSet, velSet)
%VISUALIZE - visualizes the given setup (manipulator and trajectory)

    %Globals
    global origin
    global board
    global be
    
    %pre allocation
    effPos = zeros(index, 2);       %end-effector position
    
    %plot trajectory of manipulator and dart trajectory
    for pl = 1:index         
        %function calls
        %calculate manipulators coords
        [T02, T03, T04] = forward3DoF(angleSet(1), angleSet(2), angleSet(3));
        %Coordinates of a manipulator
        links = [[[origin; T02(1:2, 4)' + origin]; T03(1:2, 4)' + origin]; T04(1:2, 4)' + origin];
        
        effPos(pl, :) = T04(1:2, 4)' + origin;
        figure(1);

        plot(board(:,1), board(:, 2), "k-", "LineWidth", 2); %board
        hold on
        plot(be(:, 1), be(:, 2), "r-", "LineWidth", 3); %bullseye
        hold on
        plot(links(:, 1), links(:, 2), 'bo-', 'LineWidth', 1, 'MarkerSize', 4.5); %manipulator
        
        %plot(effPos(:,1), effPos(:,2), 'k.-') %end-effector

        %to plot dart together with manipulator
        if(pl < index)
           hold off
        else 
           hold on
        end

        axis([-50 750 -400 400]);

        pause(0.01);

        %Update angles
        angleSet(1) = angleSet(1) + velSet(1);
        angleSet(2) = angleSet(2) + velSet(2);
        angleSet(3) = angleSet(3) + velSet(3);

    end


    %plot trajectory
    for time = 1:size(traj, 1)
        plot(traj(1:time, 1), traj(1:time, 2), "r-"); %projeсtile
        hold on
        
        %make time-accurate animation
        tr = 0.1/size(traj, 1);
        pause(tr);

        axis([-50 750 -400 400]);
    end

    pause(0.1);

end
