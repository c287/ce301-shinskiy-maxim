function [ang, vel] = decode(chromosome)
%DECODE BIT-STRING

    %Globals
    global angles
    global vels
    
    %preallocate space
    vel = zeros(1, 3);          %encoded velocities
    ang = zeros(1, 3);          %encoded angles

    %decode angles according to bit string representation
    for x = 1:3
        i_ang = dec(extractBetween(chromosome, x*3-2, x*3));
        %scale 7 actions up to 8, to fit inside 3 bit
        if(i_ang == 0)
            i_ang = 4;
        end
        ang(x) = angles(x, i_ang);
    end

    %decode velocities
    for x = 1:3
        i_vel = dec(extractBetween(chromosome, x*3+7, x*3+9));
        if(x > 2)
            vel(x) = fix((vels(i_vel+1) + 1) /2);
        else
            vel(x) = vels(i_vel + 1);
        end

    end
end