function [magnitude] = linearVelocity(o, w)
    %LINEARVELOCITY - find linear velocity from angular velocity
    
    %Global 
    global LL
    
    
    Vx = -LL(1)*sin(o(1)) * w(1) - LL(2)*sin(o(1)+o(2))*(w(1)+w(2)) - LL(3)*sin(o(1)+o(2)+o(3))*(w(1)+w(2)+w(3));
    Vy = LL(1)*cos(o(1))*w(1) + LL(2)*cos(o(1)+o(2))*(w(1)+w(2)) + LL(3)*cos(o(1)+o(2)+o(3))*(w(1)+w(2)+w(3));
    
    %Calculate the magnitude of velocity vector (speed)
    magnitude = sqrt(Vx^2 + Vy^2);
end