function TestGA
%TESTGA-complete several tests to find the most suitable configuration for
%genetic algorithm used in the experiment part

%clear command window and suspend warnings for a neat log
clc 
warning('off', 'all');

%Create diary file 
diary_file = 'GA_Optimal_Probabilities_Test.txt';
if exist(diary_file, 'file') 
    delete(diary_file);
end

%Start diary
diary(diary_file);
diary on

%Configurations to test
pop = [10];
gen = [3];
sel = [2 3 4];
xo = [0.6 0.7 0.8];
mut = [0.02 0.05 0.1];

count = 0;
log = [];
%iterate over every combination
for p = pop
    for g = gen
        for s = sel
            for x = xo
                for m = mut
                    %repeat each combination ten times for average result
                    reward = 0;
                    time = 0;
                    for iter = 1:10
                        %Run experiment
                        [ex_r, ex_t] = experimentGA(p, g, s, x, m);
                        
                        %accumulate reward and time 
                        reward = reward + ex_r;
                        time = time + ex_t;
                    end
                    %Take average result
                    count = count + 1;
                    reward = reward/10;
                    time = time/10;
                    
                    %Put into log table
                    log = [log; p, g, s, x, m, reward, time];
                    
                    %Print log to command window
                    fprintf("Iteration #%d\n", count);
                    fprintf("Сonfiguration:\n");
                    fprintf("\tn_pop = %d\n", p);
                    fprintf("\tn_gen = %d\n", g);
                    fprintf("\tn_sel = %d\n", s);
                    fprintf("\tp_xo = %.2f\n", x);
                    fprintf("\tp_mut = %.2f\n", m);
                    fprintf("\t-------------\n");
                    fprintf("\tReward (avg) = %.3f\n", reward);
                    fprintf("\tTime (avg) = %.3f\n\n", time);
                end
            end
        end
    end
end
[min_f, min_ind] = min(log(:, 6));
min_conf = log(min_ind, :);

fprintf("Total number of iterations:   %d\n", count);
fprintf("Minimum fitness = %.3f\n",min_f)
fprintf("Time taken for this configuration (avg) = %.3f\n", min_conf(7));
fprintf("Configuration:\n");
fprintf("\tn_pop = %d\n", min_conf(1));
fprintf("\tn_gen = %d\n", min_conf(2));
fprintf("\tn_sel = %d\n", min_conf(3));
fprintf("\tp_xo = %.2f\n", min_conf(4));
fprintf("\tp_mut = %.2f\n", min_conf(5));
fprintf("\t--------------\n");

diary off
end
