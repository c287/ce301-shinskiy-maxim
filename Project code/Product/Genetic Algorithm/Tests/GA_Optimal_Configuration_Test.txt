TestGA
Iteration #1
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 279.956

	Time (avg) = 0.923

Iteration #2
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 28451.476

	Time (avg) = 0.929

Iteration #3
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 188.682

	Time (avg) = 0.929

Iteration #4
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 749.620

	Time (avg) = 0.930

Iteration #5
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 611.461

	Time (avg) = 0.956

Iteration #6
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 424.078

	Time (avg) = 0.916

Iteration #7
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 541.362

	Time (avg) = 0.941

Iteration #8
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 509.373

	Time (avg) = 0.977

Iteration #9
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 256.126

	Time (avg) = 0.931

Iteration #10
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 235.863

	Time (avg) = 0.902

Iteration #11
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 253.184

	Time (avg) = 0.918

Iteration #12
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 91.687

	Time (avg) = 0.947

Iteration #13
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 478.614

	Time (avg) = 0.877

Iteration #14
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 531.674

	Time (avg) = 0.914

Iteration #15
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 111.505

	Time (avg) = 0.949

Iteration #16
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 513.806

	Time (avg) = 0.885

Iteration #17
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 377.848

	Time (avg) = 0.929

Iteration #18
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 257.349

	Time (avg) = 0.962

Iteration #19
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 381.278

	Time (avg) = 0.848

Iteration #20
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 298.691

	Time (avg) = 0.917

Iteration #21
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 317.245

	Time (avg) = 0.953

Iteration #22
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 580.881

	Time (avg) = 0.853

Iteration #23
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 301.444

	Time (avg) = 0.904

Iteration #24
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 456.427

	Time (avg) = 0.939

Iteration #25
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 93.879

	Time (avg) = 0.855

Iteration #26
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 415.637

	Time (avg) = 0.908

Iteration #27
Сonfiguration:
	n_pop = 5
	n_gen = 2
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 182.579

	Time (avg) = 0.975

Iteration #28
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 350.401

	Time (avg) = 1.108

Iteration #29
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 413.127

	Time (avg) = 1.214

Iteration #30
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 326.976

	Time (avg) = 1.258

Iteration #31
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 326.205

	Time (avg) = 1.180

Iteration #32
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 119.897

	Time (avg) = 1.233

Iteration #33
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 138.324

	Time (avg) = 1.287

Iteration #34
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 186.879

	Time (avg) = 1.166

Iteration #35
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 219.749

	Time (avg) = 1.263

Iteration #36
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 199.170

	Time (avg) = 1.253

Iteration #37
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 512.679

	Time (avg) = 1.198

Iteration #38
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 142.249

	Time (avg) = 1.243

Iteration #39
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 134.289

	Time (avg) = 1.269

Iteration #40
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 141.179

	Time (avg) = 1.165

Iteration #41
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 135.941

	Time (avg) = 1.253

Iteration #42
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 166.282

	Time (avg) = 1.291

Iteration #43
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 377.852

	Time (avg) = 1.217

Iteration #44
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 276.161

	Time (avg) = 1.242

Iteration #45
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 140.312

	Time (avg) = 1.282

Iteration #46
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 205.495

	Time (avg) = 1.074

Iteration #47
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 113.970

	Time (avg) = 1.220

Iteration #48
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 214.747

	Time (avg) = 1.300

Iteration #49
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 179.565

	Time (avg) = 1.064

Iteration #50
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 65.648

	Time (avg) = 1.246

Iteration #51
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 88.947

	Time (avg) = 1.295

Iteration #52
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 127.304

	Time (avg) = 1.103

Iteration #53
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 138.705

	Time (avg) = 1.194

Iteration #54
Сonfiguration:
	n_pop = 5
	n_gen = 3
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 278.504

	Time (avg) = 1.286

Iteration #55
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 443.619

	Time (avg) = 1.595

Iteration #56
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 202.516

	Time (avg) = 1.903

Iteration #57
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 74.996

	Time (avg) = 1.968

Iteration #58
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 284.636

	Time (avg) = 1.652

Iteration #59
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 175.006

	Time (avg) = 1.903

Iteration #60
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 79.118

	Time (avg) = 1.998

Iteration #61
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 388.144

	Time (avg) = 1.799

Iteration #62
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 105.379

	Time (avg) = 1.897

Iteration #63
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 272.562

	Time (avg) = 1.968

Iteration #64
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 125.181

	Time (avg) = 1.612

Iteration #65
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 78.416

	Time (avg) = 1.918

Iteration #66
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 82.160

	Time (avg) = 1.971

Iteration #67
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 298.379

	Time (avg) = 1.603

Iteration #68
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 144.386

	Time (avg) = 1.854

Iteration #69
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 47.010

	Time (avg) = 1.981

Iteration #70
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 231.185

	Time (avg) = 1.492

Iteration #71
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 111.464

	Time (avg) = 1.863

Iteration #72
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 39.999

	Time (avg) = 1.953

Iteration #73
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 259.119

	Time (avg) = 1.581

Iteration #74
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 160.575

	Time (avg) = 1.848

Iteration #75
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 87.983

	Time (avg) = 1.992

Iteration #76
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 112.224

	Time (avg) = 1.472

Iteration #77
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 68.686

	Time (avg) = 1.848

Iteration #78
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 78.568

	Time (avg) = 1.977

Iteration #79
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 69.539

	Time (avg) = 1.489

Iteration #80
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 193.563

	Time (avg) = 1.879

Iteration #81
Сonfiguration:
	n_pop = 5
	n_gen = 5
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 77.737

	Time (avg) = 1.997

Iteration #82
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 133.118

	Time (avg) = 1.834

Iteration #83
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 119.462

	Time (avg) = 1.898

Iteration #84
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 117.372

	Time (avg) = 1.918

Iteration #85
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 163.346

	Time (avg) = 1.881

Iteration #86
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 82.696

	Time (avg) = 1.903

Iteration #87
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 65.960

	Time (avg) = 1.919

Iteration #88
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 85.580

	Time (avg) = 1.870

Iteration #89
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 129.161

	Time (avg) = 1.888

Iteration #90
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 181.487

	Time (avg) = 1.932

Iteration #91
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 193.395

	Time (avg) = 1.866

Iteration #92
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 149.229

	Time (avg) = 1.925

Iteration #93
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 73.770

	Time (avg) = 2.043

Iteration #94
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 96.154

	Time (avg) = 1.857

Iteration #95
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 121.711

	Time (avg) = 1.899

Iteration #96
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 81.815

	Time (avg) = 1.951

Iteration #97
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 128.401

	Time (avg) = 1.870

Iteration #98
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 126.828

	Time (avg) = 1.922

Iteration #99
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 84.385

	Time (avg) = 1.942

Iteration #100
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 194.610

	Time (avg) = 1.881

Iteration #101
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 113.075

	Time (avg) = 1.921

Iteration #102
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 65.360

	Time (avg) = 1.994

Iteration #103
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 107.943

	Time (avg) = 1.879

Iteration #104
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 130.150

	Time (avg) = 1.958

Iteration #105
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 96.984

	Time (avg) = 1.969

Iteration #106
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 147.413

	Time (avg) = 1.861

Iteration #107
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 49.057

	Time (avg) = 1.969

Iteration #108
Сonfiguration:
	n_pop = 10
	n_gen = 2
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 68.441

	Time (avg) = 1.962

Iteration #109
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 68.904

	Time (avg) = 2.455

Iteration #110
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 103.299

	Time (avg) = 2.538

Iteration #111
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 101.436

	Time (avg) = 2.635

Iteration #112
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 51.461

	Time (avg) = 2.400

Iteration #113
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 102.997

	Time (avg) = 2.532

Iteration #114
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 86.280

	Time (avg) = 2.598

Iteration #115
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 107.306

	Time (avg) = 2.455

Iteration #116
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 47.180

	Time (avg) = 2.539

Iteration #117
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 84.764

	Time (avg) = 2.586

Iteration #118
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 96.577

	Time (avg) = 2.373

Iteration #119
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 59.978

	Time (avg) = 2.586

Iteration #120
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 39.810

	Time (avg) = 2.623

Iteration #121
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 140.198

	Time (avg) = 2.427

Iteration #122
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 59.565

	Time (avg) = 2.548

Iteration #123
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 81.494

	Time (avg) = 2.660

Iteration #124
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 77.113

	Time (avg) = 2.476

Iteration #125
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 92.605

	Time (avg) = 2.583

Iteration #126
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 25.310

	Time (avg) = 2.644

Iteration #127
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 213.990

	Time (avg) = 2.323

Iteration #128
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 67.916

	Time (avg) = 2.560

Iteration #129
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 55.863

	Time (avg) = 2.632

Iteration #130
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 170.317

	Time (avg) = 2.362

Iteration #131
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 81.121

	Time (avg) = 2.574

Iteration #132
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 33.955

	Time (avg) = 2.616

Iteration #133
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 195.187

	Time (avg) = 2.291

Iteration #134
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 68.283

	Time (avg) = 2.621

Iteration #135
Сonfiguration:
	n_pop = 10
	n_gen = 3
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 50.430

	Time (avg) = 2.687

Iteration #136
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 29.718

	Time (avg) = 3.511

Iteration #137
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 110.888

	Time (avg) = 3.829

Iteration #138
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 31.691

	Time (avg) = 3.958

Iteration #139
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 121.491

	Time (avg) = 3.609

Iteration #140
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 50.642

	Time (avg) = 3.860

Iteration #141
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 43.231

	Time (avg) = 3.924

Iteration #142
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 25.322

	Time (avg) = 3.609

Iteration #143
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 104.008

	Time (avg) = 3.890

Iteration #144
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 31.679

	Time (avg) = 3.958

Iteration #145
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 105.003

	Time (avg) = 3.366

Iteration #146
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 49.082

	Time (avg) = 3.862

Iteration #147
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 39.935

	Time (avg) = 3.986

Iteration #148
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 179.706

	Time (avg) = 3.447

Iteration #149
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 46.829

	Time (avg) = 3.804

Iteration #150
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 25.809

	Time (avg) = 4.019

Iteration #151
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 64.954

	Time (avg) = 3.465

Iteration #152
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 19.214

	Time (avg) = 3.876

Iteration #153
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 48.895

	Time (avg) = 4.012

Iteration #154
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 84.081

	Time (avg) = 3.544

Iteration #155
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 39.470

	Time (avg) = 3.801

Iteration #156
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 18.075

	Time (avg) = 4.014

Iteration #157
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 37.039

	Time (avg) = 3.396

Iteration #158
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 63.642

	Time (avg) = 3.838

Iteration #159
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 53.017

	Time (avg) = 4.014

Iteration #160
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 25.672

	Time (avg) = 3.512

Iteration #161
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 89.726

	Time (avg) = 3.839

Iteration #162
Сonfiguration:
	n_pop = 10
	n_gen = 5
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 22.543

	Time (avg) = 4.040

Iteration #163
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 62.710

	Time (avg) = 2.802

Iteration #164
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 56.260

	Time (avg) = 2.821

Iteration #165
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 75.421

	Time (avg) = 2.806

Iteration #166
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 46.725

	Time (avg) = 2.823

Iteration #167
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 77.781

	Time (avg) = 2.940

Iteration #168
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 77.162

	Time (avg) = 2.959

Iteration #169
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 48.224

	Time (avg) = 2.812

Iteration #170
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 40.596

	Time (avg) = 2.880

Iteration #171
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 27.508

	Time (avg) = 2.947

Iteration #172
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 70.770

	Time (avg) = 2.773

Iteration #173
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 35.484

	Time (avg) = 2.864

Iteration #174
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 44.569

	Time (avg) = 2.907

Iteration #175
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 41.989

	Time (avg) = 2.854

Iteration #176
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 45.612

	Time (avg) = 2.909

Iteration #177
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 40.876

	Time (avg) = 2.934

Iteration #178
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 62.329

	Time (avg) = 2.884

Iteration #179
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 47.854

	Time (avg) = 2.924

Iteration #180
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 45.090

	Time (avg) = 2.943

Iteration #181
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 43.492

	Time (avg) = 2.744

Iteration #182
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 64.024

	Time (avg) = 2.929

Iteration #183
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 29.178

	Time (avg) = 2.991

Iteration #184
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 89.798

	Time (avg) = 2.732

Iteration #185
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 50.762

	Time (avg) = 2.926

Iteration #186
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 18.359

	Time (avg) = 2.992

Iteration #187
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 31.940

	Time (avg) = 2.802

Iteration #188
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 33.744

	Time (avg) = 2.985

Iteration #189
Сonfiguration:
	n_pop = 15
	n_gen = 2
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 20.971

	Time (avg) = 3.017

Iteration #190
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 39.737

	Time (avg) = 3.742

Iteration #191
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 47.906

	Time (avg) = 3.763

Iteration #192
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 53.726

	Time (avg) = 3.947

Iteration #193
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 63.824

	Time (avg) = 3.833

Iteration #194
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 47.594

	Time (avg) = 3.812

Iteration #195
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 24.124

	Time (avg) = 3.920

Iteration #196
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 64.775

	Time (avg) = 3.649

Iteration #197
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 52.348

	Time (avg) = 3.890

Iteration #198
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 37.725

	Time (avg) = 3.978

Iteration #199
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 41.472

	Time (avg) = 3.572

Iteration #200
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 53.142

	Time (avg) = 3.867

Iteration #201
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 20.070

	Time (avg) = 3.954

Iteration #202
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 39.940

	Time (avg) = 3.688

Iteration #203
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 39.517

	Time (avg) = 3.950

Iteration #204
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 65.527

	Time (avg) = 3.948

Iteration #205
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 59.408

	Time (avg) = 3.611

Iteration #206
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 49.068

	Time (avg) = 3.933

Iteration #207
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 37.566

	Time (avg) = 4.022

Iteration #208
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 57.906

	Time (avg) = 3.571

Iteration #209
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 63.361

	Time (avg) = 3.802

Iteration #210
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 27.474

	Time (avg) = 3.986

Iteration #211
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 122.840

	Time (avg) = 3.482

Iteration #212
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 31.474

	Time (avg) = 3.847

Iteration #213
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 25.215

	Time (avg) = 4.019

Iteration #214
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 81.745

	Time (avg) = 3.499

Iteration #215
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 43.132

	Time (avg) = 3.914

Iteration #216
Сonfiguration:
	n_pop = 15
	n_gen = 3
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 37.483

	Time (avg) = 4.112

Iteration #217
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 38.581

	Time (avg) = 5.431

Iteration #218
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 25.642

	Time (avg) = 5.848

Iteration #219
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 2
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 29.492

	Time (avg) = 6.028

Iteration #220
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 31.347

	Time (avg) = 5.415

Iteration #221
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 17.350

	Time (avg) = 5.816

Iteration #222
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 2
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 21.296

	Time (avg) = 5.983

Iteration #223
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 29.634

	Time (avg) = 5.515

Iteration #224
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 65.597

	Time (avg) = 5.847

Iteration #225
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 13.566

	Time (avg) = 6.059

Iteration #226
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 39.502

	Time (avg) = 5.037

Iteration #227
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 49.544

	Time (avg) = 5.691

Iteration #228
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 3
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 16.462

	Time (avg) = 6.000

Iteration #229
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 44.370

	Time (avg) = 5.060

Iteration #230
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 34.151

	Time (avg) = 5.760

Iteration #231
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 3
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 25.266

	Time (avg) = 5.989

Iteration #232
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 56.170

	Time (avg) = 5.292

Iteration #233
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 29.872

	Time (avg) = 5.922

Iteration #234
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 3
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 18.310

	Time (avg) = 6.039

Iteration #235
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.02
	-------------
	Reward (avg) = 73.180

	Time (avg) = 5.137

Iteration #236
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.05
	-------------
	Reward (avg) = 33.803

	Time (avg) = 5.693

Iteration #237
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 4
	p_xo = 0.60
	p_mut = 0.10
	-------------
	Reward (avg) = 21.756

	Time (avg) = 6.051

Iteration #238
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.02
	-------------
	Reward (avg) = 90.378

	Time (avg) = 5.073

Iteration #239
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.05
	-------------
	Reward (avg) = 52.408

	Time (avg) = 5.811

Iteration #240
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 4
	p_xo = 0.70
	p_mut = 0.10
	-------------
	Reward (avg) = 20.980

	Time (avg) = 6.043

Iteration #241
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.02
	-------------
	Reward (avg) = 87.521

	Time (avg) = 5.349

Iteration #242
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.05
	-------------
	Reward (avg) = 45.434

	Time (avg) = 5.760

Iteration #243
Сonfiguration:
	n_pop = 15
	n_gen = 5
	n_sel = 4
	p_xo = 0.80
	p_mut = 0.10
	-------------
	Reward (avg) = 27.987

	Time (avg) = 6.086

Total number of iterations:   243
Minimum fitness = 13.566
Time taken for this configuration (avg) = 6.059
Configuration:
	n_pop = 15
	n_gen = 5
	n_sel = 2
	p_xo = 0.80
	p_mut = 0.10
	--------------