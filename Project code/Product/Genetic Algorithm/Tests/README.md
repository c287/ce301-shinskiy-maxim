### Test

[TestGA.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/f3d8f1406ddb79a79bdbc49f0836ae56bcc2541f/Project%20code/Product/Genetic%20Algorithm/Tests/TestGA.m) function is used to to find the optimal properties for genetic algorithms by testing them out.  The results of the testing was judged based on the reward and time that the test scored on average after 10 iterations. To log the messages of the program, it uses MatLab `diary` function.



##### Configuration 

By configuration it is meant the parameters of genetic algorithm other than probabilities, nevertheless probability parameters were also tried out. This test was ran to find out suitable configuration to use in the experiment. Following ranges were used in the test:
```math
5 \le n\_pop \le 15\\
2 \le n\_gen \le 5\\
2 \le n\_sel \le 4\\
0.6 \le p\_xo \le 0.8\\
0.02 \le p\_mut \le 0.1\tag{1}
```
The borders of these ranges were set by means of common sense; the experiment should not take too long and therefore number of of population and generations should be kept relatively low. A [text file](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/f3d8f1406ddb79a79bdbc49f0836ae56bcc2541f/Project%20code/Product/Genetic%20Algorithm/Tests/GA_Optimal_Configuration_Test.txt) was produced stating the best configuration (judged from fitness solely), however it was decided to choose a configuration that takes less time. It can be noticed that configurations with `n_pop = 10` and  `n_gen = 3` take about 2.5 seconds to complete even though the resulting fitness was decent, hence it was decided to use a configuration `n_pop = 10, n_gen = 3, n_sel = 2`. 



##### Probabilities 

Probability test can be done independently from configuration because the results are only judge from resulting fitness and not time, regardless the optimal configuration from previous was used. 

The probabilities and `n_sel` was used the same as in figure 1. As an outcome a [text log file](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/33216760c60c093d4608cbbdf08b71986de42cf7/Project%20code/Product/Genetic%20Algorithm/Tests/GA_Optimal_Probailities_Test.txt) shows that `n_sel = 4, p_xo = 0.8, p_mut = 0.1` are the parameters that on average performed better than others. Therefore these parameters are used as default in experimentGA.m.

##### Remarks

Although the testing has been done to find out the "most optimal configuration", the outcome is not necessary is optimal, in the end of the day each configuration was only ran for 10 times and statistics were limited to mean fitness and mean time.

