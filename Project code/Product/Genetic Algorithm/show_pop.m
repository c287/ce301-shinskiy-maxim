function show_pop()
%DISPLAY ALL POPULATION

    %Globals 
    global n_pop
    global pop_i
    global pop_f

    f_avg = 0;
    for i_p = 1:n_pop
        [ang, vel] = decode(pop_i(i_p));
        fprintf('%18s    ', pop_i(i_p));   %genotype
        fprintf('%4d %4d %4d    %2d %2d %2d   ', ang(1), ang(2), ang(3), vel(1), vel(2), vel(3));   %phenotype
        fprintf('%.1f \n', pop_f(i_p));   %fitness
        if(~isnan(pop_f(i_p)))
            f_avg = f_avg + pop_f(i_p);
        end
    end
    fprintf('---------------------------------------------------------\n');
    fprintf('Avg fitness: %4.1f\n', f_avg/n_pop);
    fprintf('-------------------\n');


end