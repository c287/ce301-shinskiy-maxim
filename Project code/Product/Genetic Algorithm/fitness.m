function [fit] = fitness(chromosome, draw)
%CALCULATE FITNESS

    %Globals
    global pop_i
    global pop_f
    
    global S

    global origin
    global board

    %preallocate space
    effPos = zeros(S, 2);       %end-effector position
    secJointPos = zeros(S, 2);  %seconds joint position

    indx = pop_i == chromosome;

    %obtain angles and velocities
    [ang, vel] = decode(chromosome);

    if(draw)
        ang_init = ang;
        vel_init = vel;
    end


    %calculate links coords
    for step = 1:S
        %calculate manipulators coords
        [~, T03, T04] = forward3DoF(ang(1), ang(2), ang(3));  %forward kinematics

        %Store end-effector position
        eff = T04(1:2, 4)' + origin;
        sj = T03(1:2, 4)' + origin;

        effPos(step, :) = eff;
        secJointPos(step, :) = sj;

        %Update angles
        for x = 1:3
            ang(x) = ang(x) + vel(x);
        end
    end
    %find angle that can be thrown at a tangent to the trajectory and
    %towards the target
    [suitAngle, index] = findSuitableAngle(board(2, 1), board(2, 2), effPos, secJointPos);

    %find the linear velocity at end-effector
    linVel = linearVelocity(ang, vel);
    %calculate trajectory and optimal angle
    [traj, t] = forcedTrajectory(linVel, suitAngle, effPos(index, 1), effPos(index, 2));

    %calculate distance to bullseye for reward function
    if(size(traj, 1) > 1)
        d = sqrt(traj(end, 1)^2 + traj(end, 2)^2);
    else
        d = sqrt(origin(1)^2 + origin(2)^2);
    end

    %reward for the trajectory
    fit = d^2;
    pop_f(indx) = fit;

    if(draw)
       fprintf('Best properties:\n\t angle 1 = %d\n\t angle 2 = %d\n\t angle 3 = %d\n\t vel 1 = %d\n\t vel 2 = %d\n\t vel 3 = %d\n', ang_init(1), ang_init(2), ang_init(3), vel_init(1), vel_init(2), vel_init(3))
       visualize(traj, index, ang_init, vel_init);
    end
end