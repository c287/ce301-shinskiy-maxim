function [T02, T03, T04] = forward3DoF(theta1, theta2, theta3)
%FORWARD3DOF function to calculate coords of a manipulator
%   This function calculates the points of each joint and end effector
%   so they can be then plotted

global LL;

%General params
alpha = 0;
%Store to avoid recalculation
cosdA = cosd(alpha);
sindA = sind(alpha);
d = 0;

    function T = transform(theta, a)
        
        %Store to avoid recalculation
        cosdT = cosd(theta);
        sindT = sind(theta);
        
        %General transform for [i-1][i]T
        T = [cosdT -sindT 0 a
            sindT*cosdA cosdT*cosdA -sindA -sindA*d
            sindT*sindA cosdT*sindA cosdA cosdA*d
             0 0 0 1];
    end


%01T
T01 = transform(theta1, 0);

%12T
T12 = transform(theta2, LL(1));

%23T
T23 = transform(theta3, LL(2));

%34T
T34 = transform(0, LL(3));

%Return
T02 = T01*T12;
T03 = T02*T23;
T04 = T03*T34;
end
