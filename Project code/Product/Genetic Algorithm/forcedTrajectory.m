function [traj, time] = forcedTrajectory(velocity, angle, x, y)
%FORCEDTRAJECTORY calculates the trajectory of a thrown dart until xPos < 0
    traj = [];

    g = 9.81; %gravity pull
    %translate to (0, 0) base system
    x = -x;
    y = -y;
    h = 100; %height below which dart shouldn't go (end of dartboard)
    v = velocity;
    
    %time of flight   
    syms t
    t = solve((v*sind(angle)*t - (g*t^2)/2) == y - h, t);
    t = double(max(t));
    
    for time = 0:t/250:t 
        posX = double(v*time*cosd(angle) - x);%X at time T
        posY = double(v*time*sind(angle) - (g*time^2)/2 - y);%Y at time T

        if(posX > 0)
            traj = [traj; [posX posY]]; 
        end
    end
end