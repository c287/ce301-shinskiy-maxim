function [suitAngle, index] = findSuitableAngle(targetX, targetY, effPos, secJointPos)
%FINDSUITABLEANGLE - find the angle that is a smallest difference between
%taget-gripper line and perpendicular to gripper line

    %preallocation
    diffAngles = zeros(1, size(effPos, 1));
    aimAngles = zeros(1, size(effPos, 1));
    baseAngles = zeros(1, size(effPos, 1));
    
    %iterate over a list of eff
    for i = 1:size(effPos, 1)

        %aim angle - perpendicular to gripper
        aimAngle = 90 + atan2d((effPos(i, 2) - secJointPos(i, 2)) , (effPos(i, 1) - secJointPos(i, 1)));

        %base angle, between end-effector and taget bullseye
        baseAngle = 180 + atan2d((effPos(i, 2) - targetY),(effPos(i, 1) - targetX));

        %diffAngles = difference in base and aim
        %ensure that dart is facing target
        if(aimAngle > 90)
            diffAngles(i) = baseAngle - aimAngle;
        else 
            %input large number so it won't be picked later
            diffAngles(i) = 10000;
        end
        %tangent to the gripper (log of aim angles)
        aimAngles(i) = aimAngle;
    end
    
    %pick the angle with smallest difference
    [~, index] = min(abs(diffAngles));
    suitAngle = aimAngles(index);
end

