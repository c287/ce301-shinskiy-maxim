function [min_f, time_taken] = experimentGA(pop, gen, sel, xo, mut)
%Steade state Genetic Algorithm intended to find the optimal angles and
%angular velocities for 3dof manipulator, using binary representaion

    tic
    close all;
    
    COMMENTS = true;
    %Define default configuration when function is run with no arguments 
    if nargin < 1
        pop = 10;
        gen = 3;
        sel = 2;
        xo = 0.8;
        mut = 0.10;
    %When function is run with arguments
    elseif nargin == 5
        %disable comments
        COMMENTS = false;
    else 
        error("Not enough arguments!\nExpected usage: experimentGA() or experimentGA(pop, gen, sel, xo, mut).")
    end
    
  
    %Globals
    global LL
    
    global origin
    global board
    global be
    
    global S
  
    global angles
    global vels
    
    global n_pop
    global n_sel
    
    global pop_i
    global pop_f
    
    %Manipulator constants
    %Link lengths
    LL = [50, 40, 15];

    %Coordinates  
    origin = [700 -100];        %Origin of manipulator
    board = [0 -25; 0 0; 0 25]; %Board coord for plot
    be = [0 -2; 0 0; 0 2];      %BullsEye
    
    %Preallocating arrays
    S = 25;                     %number of steps 
    
    %Discrete action spaces
    vels = [1 2 3 4 5 6 7 8];         %velocity
    angles = [75 80 85 90 95 100 105    %angles
        -120 -125 -130 -135 -140 -145 -150
        -75 -80 -85 -90 -95 -100 -105];

    
    %GA Representation constants
    n_bits = 18;    % a1, a2, a3, v1, v2, v3 := 3-3-3-3-3-3
    n_pop = pop;    %population size
    n_gen = gen;    %generation size
    n_sel = sel;    %tournament size
    %probabilities
    p_xo = xo;     %crossover per individual
    p_mut = mut;   %mutation per bit

    %Create empty initial generation
    pop_i = string(zeros(n_pop, 1));    %pop of individuals
    pop_f = zeros(n_pop, 1);    %pop of fitnesses of corresponding individuals
    
    %Build a binary string
    for i = 1:n_pop
        c = "";
        for j = 1:n_bits
            c = strcat(c, int2str(randi(2)-1));
        end
        pop_i(i) = c;
        pop_f(i) = NaN;
    end
    pop_i = string(pop_i);
    if COMMENTS
        fprintf('Configuration:\n');
        fprintf('\tPopulation:     %d\n', n_pop);
        fprintf('\tGenerations:    %d\n', n_gen);
        fprintf('\tTourn. select.: %d\n', n_sel);
        fprintf('\tXO. prob.:      %.2f\n', p_xo);
        fprintf('\tMutation prob.: %.2f\n\n', p_mut);

        fprintf('Initial Population:\n');
        fprintf('     Genotype            angles       velocities  fitness\n');
        fprintf('---------------------------------------------------------\n');
        show_pop();
    end
    %main loops
    for generation = 1:n_gen
        for individual = 1:n_pop
            if(unifrnd(0, 1) < p_xo)
                %crossover
                index_i_1 = tournament(true);
                index_i_2 = tournament(true);
                i_1 = pop_i(index_i_1);
                i_2 = pop_i(index_i_2);
                %pick cut position and get offspring
                cut = randi([2, n_bits - 2]);
                offspring_p1 = extractBetween(i_1, 1, cut);
                offspring_p2 = extractBetween(i_2, cut + 1, n_bits);
                offspring = strcat(offspring_p1, offspring_p2);
            else
                %cloning 
                index_i_1 = tournament(true);
                offspring = pop_i(index_i_1);
            end

            %mutation
            mutation = '';
            offspring = convertStringsToChars(offspring);   %to be able to index
            for i_mut = 1:n_bits
                if(unifrnd(0, 1) < p_mut)
                    %flip one bit
                    if(offspring(i_mut) == '0')
                        mutation = strcat(mutation, '1');
                    else
                        mutation = strcat(mutation, '0');
                    end
                else
                    mutation = strcat(mutation, offspring(i_mut));
                end
            end

            %replace weakest in selected tournament
            offspring = convertCharsToStrings(mutation);
            i_weakest = tournament(false);
            pop_i(i_weakest) = offspring;
            i_off = pop_i == offspring;
            if(i_off)
                %keep the offsprings fitness if it is already evaluated
                if(~isnan(pop_f(i_off)))
                    %filter out useless values
                    f_off = pop_f(i_off);
                    f_off = f_off(~isnan(f_off));
                    f_off = f_off(f_off>0);
                    f_off = f_off(1);
                    pop_f(i_weakest) = f_off;   
                end
            else
                %put NaN if it is new and do not evaluate fitness just yet
                pop_f(i_weakest) = NaN;
            end
        end
        if COMMENTS
            fprintf('Generation:  #%d\n',generation);
            fprintf('     Genotype            angles       velocities  fitness\n');
            fprintf('---------------------------------------------------------\n');
            show_pop();
        end
    end    
    % visualize best individual
    [min_f, min_ind] = min(pop_f);
    min_i = pop_i(min_ind);
    time_taken = toc;
    if COMMENTS
        fprintf('Best fitness: %.3f\n', min_f);
        fitness(min_i, true);
        fprintf("Solution found in %.2f second(s).\n", time_taken);
    end
end
