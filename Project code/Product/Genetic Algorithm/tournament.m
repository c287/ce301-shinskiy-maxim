function [index] = tournament(inverse)
%TOURNAMENT SELECTION

    %Globals 
    global n_pop
    global n_sel
    
    global pop_i
    global pop_f

    %Constants
    index = 0;
    f_max = -Inf;
    f_min = Inf;

    for count = 1:n_sel
        %Get random individual index
        index_i = randi(n_pop);
        %Get actual individual
        chromosome = pop_i(index_i);
        %fitness of that individual
        if(isnan(pop_f(index_i)))
            f_i = fitness(chromosome, false);
        else 
            f_i = pop_f(index_i);
        end

        %Do tournament
        if(inverse == false)
           if(f_i > f_max)
              f_max = f_i;
              index = index_i;
           end
        else
            if(f_i < f_min)
                f_min = f_i;
                index = index_i;
            end
        end
    end
end