function [decimal] = dec(chromosome)
%BINARY TO DECIMAL CONVERTION

    decimal = 0;
    chromosome = convertStringsToChars(chromosome);
    for i_dec = 1:size(chromosome, 2)
        i_bi = size(chromosome, 2) - i_dec;
        if(chromosome(i_dec) == '1')
            decimal = decimal + 2^i_bi;
        end
    end
end