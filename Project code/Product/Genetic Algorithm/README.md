### Finding a Solution with Genetic Algorithm

This directory presents the product of the project, a Dart Throwing Robot.

#### Experiment

This is a final version of previously developed [experimentRL.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/284e19c673d459960b5c9a540855f4bc1f275089/Project%20code/Development/Machine%20Learning/Code/experimentRL.m) and [experiment.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/284e19c673d459960b5c9a540855f4bc1f275089/Project%20code/Development/Machine%20Learning/Code/experiment.m). Function takes a discrete action space and finds an optimal solution within a sensible amount of time.

The difference with previous versions is that now the solution is found within a short time using a searching algorithm rather a brute force. Function makes use of steady-state binary genetic algorithm that was developed for the purpose of finding solution.

##### Genetic Algorithm

 It is to be said that the function doesn't learn and therefore each consecutive run has no effect on the result. Instead the solution is found by specific selection of angle-velocity configurations to form a next generation and fill it with new permutated or unchanged configurations.

The process starts from populating an initial generation. A generation is a group of individuals of one size. These individuals are angle-velocity configurations and are encoded into a bit-string. A bit-string is a set of bits, in this case every individual has 18 bits and every three bits represent one value, e.g. `100110101011011001` will be decoded as `4-6-5-3-3-1` and will correspond to $`\theta_1 - \theta_2 - \theta_3 - \omega_1 - \omega_2 - \omega_3`$. Every value is an index in action space. The individuals for a population are generated randomly and a size of a population is defined by a constant `n_pop` which is one of the properties of genetic algorithm. To create next generation, algorithm either clones an individual from a previous generation or crosses two individuals to create a new offspring. The probability of crossing happening is set by `p_xo` constant. The individuals for, so called crossover, are picked through a tournament selection. This means that some number (`n_sel`) of individuals are randomly chosen from a previous generation and the one with a better fitness (the lower the better) is selected to be a winner and at this point the winner gets to be chosen for a crossover. One point crossover is a process at which two individuals give some part of their bit-string to form a new individual (offspring) as seen in figure 1. The cut point is chosen randomly, but in a way that at least 1 bit is given off by one of the individuals.

| ![Crossover Diagram](/doc%20src/xo_diagram.png "Figure 1") |
| :----------------------------------------------------------: |
|                     Figure 1. Crossover                      |

Else, in case of cloning, the individual that wins the tournament is cloned. Before putting sn offspring into a new generation it may be chosen for a bit-flip mutation. The mutation probability is set by `p_mut` constant and this probability is per bit and not per individual. Bit-flip means that if the mutation is happening to a `1` bit then it becomes `0` and vice versa. This mutation allows an algorithm to explore more variations with suitable GA parameters. After this stage an individual replaces another individual in a generation, that is chosen (an individual) to be the weakest by an inverse tournament selection, a selection where the winner has the worst (largest) fitness. This process is running for `n_gen` number of generations and as a result, an individual with best (lowest) fitness is a solution.

[GA function](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/44d5488e37e4b966e9581a5bc86b6c7823719904/Project%20code/Product/Genetic%20Algorithm/experimentGA.m) has two constructors: with no and with five arguments that specify genetic algorithm parameters. If ran with no arguments, genetic algorithm uses default predefined properties. When ran with specified properties, the comments (statistics) are disabled. This is needed for a [test function](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Product/Genetic%20Algorithm/Tests). For simplicity, all constants are shared as global variables between the functions.

As like in experimentRL.m the angle action space is the same, however velocity action space has changed a bit, $`v_1`$ and $`v_2`$ have a choice from 1 to 8 degree/sec but $`v_3`$ - 1 to 4. This is made to limit the resulting linear velocity of a dart.

The generation is implemented as two arrays: string array (`pop_i`) and double array (`pop_f`). String array stores a population of individuals as bit-strings, they are stored as a string value for convenience,  for easier bit-string manipulations (e.g. iteration, cutting, indexing) string value is converted into char-array. Evaluated fitness is stored in `pop_f` at the index that corresponds to the index of that individual in `pop_i`.  MatLab doesn't allow storing different datatypes in one array, so this way same index can be used to access both: bit-string and it's fitness. 

To optimize the program in terms of time, genetic algorithm only evaluates fitness, if at a stage of tournament selection is has not yet been evaluated. Every bit-string put in the initial generation has corresponding fitness value of `NaN`. When a new individual is put into a generation, it is compared to others, with a help of logic vectors, and if there is a same bit-string already present it's fitness is copied to new individual. Fitness evaluation function is the most costy due to both robot links trajectory and dart trajectory calculations involved, therefore minimising the number of times a fitness needs to be evaluated increases the performance drastically.



#### Supporting Functions

As far as this program uses modified and final versions of previously developed programs and function, many of these functions were reused or changed just slightly.

##### Binary to Decimal Conversion: [dec.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/44d5488e37e4b966e9581a5bc86b6c7823719904/Project%20code/Product/Genetic%20Algorithm/dec.m)

Simple convertor, that is used to convert parts of a bit-string into decimal values. 



##### Bit-string decoding: [decode.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/44d5488e37e4b966e9581a5bc86b6c7823719904/Project%20code/Product/Genetic%20Algorithm/decode.m)

Two consecutive for-loops take a three bit part of an individual and retrieve a value that corresponds to the decimal index value, that is encoded in that three bits. First nine bits represent angles and last 9 - velocities. 

Angle action space has a choice of 7 angles, however three bits can encode 8, therefore the action space is upscaled to fit that; this way index 4 has a higher probability of occurring in the bit string because is has two values that corresponds to it: `000` and `100`.



##### Release Point Decision: [findSuitableAngle.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/44d5488e37e4b966e9581a5bc86b6c7823719904/Project%20code/Product/Genetic%20Algorithm/findSuitableAngle.m)

This function was left unchanged from development and is described [here](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Development/Machine%20Learning/Code).



##### Fitness Function: [fitness.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/44d5488e37e4b966e9581a5bc86b6c7823719904/Project%20code/Product/Genetic%20Algorithm/fitness.m)

Fitness function calculates the trajectory of the manipulator and a dart, then calculates the fitness from resulting landing position. It also has an option of plotting the outcoming trajectories and is specified as a boolean argument `draw`. "Draw" option is used in the end of the program to plot the solution, it processes supplied bit-string the same way as in previous versions of experiment program and plots the result in the end with a message that tells the configuration. 

Chromosome is another term for a bit-string and is used here as a variable that contains a bit-string. Comparing it to a population will form a vector index `indx`. Then function calculates the trajectories in same way as [experimentRL.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/44d5488e37e4b966e9581a5bc86b6c7823719904/Project%20code/Development/Machine%20Learning/Code/experimentRL.m). As a result the trajectory might be invalid if, for example, the dart flies in the wrong direction, then the landing point is considered to be the origin. Otherwise, the fitness is a distance to the target from the landing point and therefore the smaller the fitness the better. To emphasise the distance, the fitness is squared and then put into fitness array to replace the fitness value of all same chromosomes, this is done with the index vector from the beginning of the function. 



##### Trajectory of a Dart: [forcedTrajectory.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/be6308dbfb34bfdc750b039aa5e1dddf0e0b7c20/Project%20code/Product/Genetic%20Algorithm/forcedTrajectory.m)

The function is taken from previously developed one, nevertheless the only changed happened is that the trajectory is now calculated at the rate of 250 points per trajectory. The function is described [here](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Development/Machine%20Learning/Code).



##### Forward Function: [forward3DoF.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/be6308dbfb34bfdc750b039aa5e1dddf0e0b7c20/Project%20code/Product/Genetic%20Algorithm/forward3DoF.m)

Function was adapt into a experiment by means of using globals to share constants (`LL`). Other than that, it was kept unchanged. The functionality is explained [here](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/121f95c6093f5f3cdb6b5b90e1e79f2418541717/Project%20code/Development/3DoF).



##### Angular to Linear: [linearVelocity.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/be6308dbfb34bfdc750b039aa5e1dddf0e0b7c20/Project%20code/Product/Genetic%20Algorithm/linearVelocity.m)

To fit into experiment program, function's locals were replaced by globals to decrease the  number of arguments of a function. The function described [here](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Development/Machine%20Learning/Code).



##### Print Population: [show_pop.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/be6308dbfb34bfdc750b039aa5e1dddf0e0b7c20/Project%20code/Product/Genetic%20Algorithm/show_pop.m)

Function lists all individuals' genotype (bit-string) and phenotype (corresponding angles and velocities) and calculates and prints average fitness of non-nan population.



##### Tournament Selection: [tournament.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/be6308dbfb34bfdc750b039aa5e1dddf0e0b7c20/Project%20code/Product/Genetic%20Algorithm/tournament.m)

Performs tournament selection meaning picking smallest/largest fitness of individuals in a group. The function uses for-loop to randomly pick an individual out of generation and evaluates it's fitness if it has not yet been calculated. Then, still inside the for-loop, it compares a newly picked fitness value with previously stored as minimum/maximum and replaces if new one is less/greater. It compares `n_sel` number of individuals. The function works both ways: finds both best and worst, to decide the flow of logic the function has a boolean argument `inverse` and if it is `true` then the output is going to be the index of the smallest fitness. 



##### Experiment Visualization: [visualize.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/be6308dbfb34bfdc750b039aa5e1dddf0e0b7c20/Project%20code/Product/Genetic%20Algorithm/visualize.m)

Together with few others, this function was adapted by using global variable to fit the main program. Function is explained [here](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Development/Machine%20Learning/Code).

