function [reward] = rewardFunction(d, t)
%REWARDFUNCTION calculates the reward for given distance and time of flight
kT = 0;     %defines how much the time affects the reward
kD = 5000;  %defines how much the distance affects the reward

timeVar = kT/t; %less time, bigger reward
distVar = kD/d; %less distance bigger reward

reward = timeVar + distVar^2; %function
end