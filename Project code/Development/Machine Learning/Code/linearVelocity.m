function magnitude = linearVelocity( o1, o2, o3, w1, w2, w3, L1, L2, L3)
    %LINEARVELOCITY - find linear velocity from angular velocity
    Vx= -L1*sin(o1) * w1 - L2*sin(o1+o2)*(w1+w2) - L3*sin(o1+o2+o3)*(w1+w2+w3);
    Vy= L1*cos(o1)*w1 + L2*cos(o1+o2)*(w1+w2) + L3*cos(o1+o2+o3)*(w1+w2+w3);
    
    %Calculate the magnitude of velocity vector (speed)
    magnitude = sqrt(Vx^2 + Vy^2);
end