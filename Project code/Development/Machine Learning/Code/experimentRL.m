%Program to generate an expirement with discrete state.

close all
clear all

%run time
tic
total_time = 0;

%for loops
N = 1000;
S = 20;

%end-effector position
effPos = zeros(S, 2);

%seconds joint position
secJointPos = zeros(S, 2);

%results table
table = zeros(N, 7);

%Links
L1 = 50;
L2 = 40;
L3 = 15;
%Link lengths
LL = [L1, L2, L3];

%Origin of manipulator
origin = [500 -90];

%Board coord for plot
board = [0 -25; 0 0; 0 25];
be = [0 -2; 0 0; 0 2]; %Bullseye

%Velocities choice
vels = [1 2 3 4 5 6 7 8 9 10   
        1 2 3 4 5 6 7 8 9 10  
        1 1 2 2 3 3 4 4 5 5];

%Angles choice
angles = [75 80 85 90 95 100 105
        -120 -125 -130 -135 -140 -145 -150
        -75 -80 -85 -90 -95 -100 -105];

%repeat experiment N times
for n = 1:N
    
    %Pick angles and velocities to test
    angleChoice = [angles(1, randi(7)); angles(2, randi(7)); angles(3, randi(7))];
    velChoice = [vels(1, randi(9)); vels(2, randi(9)); vels(3, randi(9))];  
    
    angle1 = angleChoice(1);
    angle2 = angleChoice(2);
    angle3 = angleChoice(3);
    
    vel1 = velChoice(1);
    vel2 = velChoice(2);
    vel3 = velChoice(3);

    %for S steps 
    for step = 1:S
        %functions calls
        %calculate manipulators coordinates
        [T02, T03, T04] = forward3DoF(L1, L2, L3, angle1, angle2, angle3);
        links = [[[origin; T02(1:2, 4)' + origin]; T03(1:2, 4)' + origin]; T04(1:2, 4)' + origin];
        
        %Store position of last link (both ends)
        eff = T04(1:2, 4)' + origin;
        sj = T03(1:2, 4)' + origin;
        
        effPos(step, :) = eff;
        secJointPos(step, :) = sj;
        
        %Update angles
        angle1 = angle1 + vel1;
        angle2 = angle2 + vel2;
        angle3 = angle3 + vel3;
    end
    
    %find angle that is closer to 45deg from end-effectors position
    [suitAngle, index] = findSuitableAngle(board(2, 1), board(2, 2), effPos, secJointPos);
    
    %find the linear velocity at end-effector
    linVel = linearVelocity(angle1, angle2, angle3, vel1, vel2, vel3, L1, L2, L3);
    fprintf('Run #%d', n);
    fprintf(newline);
    fprintf('Throw: speed = %4.1f, angle = %3.1f', linVel, suitAngle);
    fprintf(newline);
    
    %calculate trajectory and optimal angle
    [traj, t] = forcedTrajectory(linVel, suitAngle, effPos(index, 1), effPos(index, 2));
        
    %calculate distance to bullseye for reward function
    d = sqrt(traj(end, 1)^2 + traj(end, 2)^2);
    
    %reward for the trajectory
    reward = rewardFunction(d, t);
    
    %store sets(angle, vel, reward)
    angleSet = [angleChoice(1), angleChoice(2), angleChoice(3)];
    velocitySet = [velChoice(1), velChoice(2), velChoice(3)];
    
    table(n, :) = [angleSet, velocitySet, reward];

    %print message
    fprintf('Reward = %5.2f, time = %3.1f, dist = % 4.1f', reward, t, d);
    fprintf(newline);
    
    %visualize the manipulator throwing a darts 
    %comment out for time efficiency
    %args - visualize(traj, index, LL, angle1, angle2, angle3, vel1, vel2, vel3)
    visualize(traj, index, LL, angleSet, velocitySet)
    total_time = total_time + toc;
    avg_time = total_time/n;
    fprintf(newline);
end



