%Program to generate an expirement with random state.

%Links
L1 = 50;
L2 = 40;
L3 = 15;

%Origin of manipulator
origin = [500 -100];

%Board coord for plot
board = [0 -25; 0 0; 0 25];
be = [0 -2; 0 0; 0 2]; %BullsEye

%Generate random state
for i = 1:10
    %[theta1, theta2, theta3, initial velocity] 
    % 90-180, 0-(-165), 0-30, 50-150 <--Ranges
    % pick reasonable numbers to include joint limits and possible dart speed
    % in degress and cm/s respectively
    randState(i, :) = [90 + randi(90),  -randi(165), randi(30), 50 + randi(100)]; 
end


for rs = 1:length(randState)
    close all
    
    %Message
    %message = ['joint 1', 'joint 2', 'joint 3', 'speed']
    disp(['Experiment: ', num2str(rs)])
    fprintf('%8s ', 'joint1', 'joint 2', 'joint 3', 'speed');
    fprintf(newline);
    fprintf('%8d ', randState(rs, :));
    fprintf(newline);
    
    %function calls
        %calculate manipulators coords
    [T02, T03, T04] = forward3DoF(L1, L2, L3, randState(rs, 1), randState(rs, 2), randState(rs, 3));
        %Coordiante of a manipulator
    links = [[[origin; T02(1:2, 4)' + origin]; T03(1:2, 4)' + origin]; T04(1:2, 4)' + origin];
        %calculate trajectory
    [traj, t] = forcedTrajectory(randState(rs, 4), randState(rs, 3) + 90, links(4, 1), links(4, 2));
   
    d = sqrt(traj(end, 1)^2 + traj(end, 2)^2);  %distance to bullseye
    
    rewardVar = rewardFunction(d, t);
    fprintf('Reward = %5.2f, time: %3.1f, dist: % 4.1f', rewardVar, t, d);
    fprintf(newline);
    disp("-----------------------------------------");
    figure(1);
    
    plot(board(:,1), board(:, 2), "k-", "LineWidth", 2); %board
    hold on
    plot(be(:, 1), be(:, 2), "r-", "LineWidth", 3); %bullseye
    hold on
    plot(limbs(:, 1), links(:, 2), 'bo-'); %manipulator
    hold on
    axis([-100 600 -350 350]);
    
    %pause;
    %plot trajectory
    for time = 1:size(traj, 1)
        plot(traj(1:time, 1), traj(1:time, 2), "r-"); %projcetile
        hold on
        pause(0.01);
    end
    
    %wait
    pause(2)
end
