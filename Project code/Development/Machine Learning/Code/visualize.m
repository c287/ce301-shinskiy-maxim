function [] = visualize(traj, index, LL, angleSet, velSet)
%VISUALIZE - visualizes the given setup (manipulator and trajectory)

    %Environment objects
    %Origin of manipulator
    origin = [500 -90];

    %Board coord for plot
    board = [0 -25; 0 0; 0 25];
    be = [0 -2; 0 0; 0 2]; %BullsEye
    
    %plot trajectory of manipulator and ~45deg dart trajectory
    for pl = 1:index         
        %function calls
        %calculate manipulators coords
        [T02, T03, T04] = forward3DoF(LL(1), LL(2), LL(3), angleSet(1), angleSet(2), angleSet(3));
        %Coordinates of a manipulator
        links = [[[origin; T02(1:2, 4)' + origin]; T03(1:2, 4)' + origin]; T04(1:2, 4)' + origin];

        figure(1);

        plot(board(:,1), board(:, 2), "k-", "LineWidth", 2); %board
        hold on
        plot(be(:, 1), be(:, 2), "r-", "LineWidth", 3); %bullseye
        hold on
        plot(links(:, 1), links(:, 2), 'bo-'); %manipulator

        %to plot dart together with manipulator
        if(pl < index)
           hold off 
        else 
           hold on
        end

        axis([-100 600 -350 350]);

        pause(0.01);

        %Update angles
        angleSet(1) = angleSet(1) + velSet(1);
        angleSet(2) = angleSet(2) + velSet(2);
        angleSet(3) = angleSet(3) + velSet(3);

    end


    %plot trajectory
    for time = 1:size(traj, 1)
        plot(traj(1:time, 1), traj(1:time, 2), "r-"); %projeсtile
        hold on
        %pause to create an accurate time scale (250 is time resolution in
        %forcedTrajectory function
        %p = double(t)/250;
        tr = 0.1/size(traj, 1);
        pause(tr);

        axis([-100 600 -350 350]);
    end

    pause(0.1);

end
