function [suitAngle, index] = findSuitableAngle(targetX, targetY, effPos, secJointPos)
%FINDSUITABLEANGLE - find the angle that is closest to 45 deg perpendicular
%to the grippper

    %preallocation
    diffAngles = zeros(1, size(effPos, 1));
    aimAngles = zeros(1, size(effPos, 1));
    baseAngles = zeros(1, size(effPos, 1));
    
    %iterate over a list of eff
    for i = 1:size(effPos, 1)

        %current aim angle in rads atan(y2-y1 / x2-x1) + pi, this is a
        %tangent to the end effector
        aimAngle = 90 + atan2d((effPos(i, 2) - secJointPos(i, 2)) , (effPos(i, 1) - secJointPos(i, 1)));

        %base angle in rads
        baseAngle = 180 + atan2d((effPos(i, 2) - targetY),(effPos(i, 1) - targetX));

        %diffAngles = difference in base and aim
        %convert to degrees for output
        if(aimAngle > 90)
            diffAngles(i) = baseAngle - aimAngle;
        else 
            %input large number so it won't be picked later
            diffAngles(i) = 10000;
        end
        %tangent to the gripper (log of aim angles)
        aimAngles(i) = aimAngle;
    end
    
    %pick the angle closest to 45 deg
    [~, index] = min(abs(diffAngles));
    suitAngle = aimAngles(index);
end

