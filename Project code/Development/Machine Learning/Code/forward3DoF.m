function [T02, T03, T04] = forward3DoF(L1, L2, L3, theta1, theta2, theta3)
%FORWARD3DOF function to calculate coords of a manipulator
%   This function calculates the points of each joint and end effector
%   so they can be then plotted

%General params
alpha = 0;
d = 0;

    function T = transform(theta, a)
        %General transform for [i-1][i]T
        T = [cosd(theta) -sind(theta) 0 a
            sind(theta)*cosd(alpha) cosd(theta)*cosd(alpha) -sind(alpha) -sind(alpha)*d
            sind(theta)*sind(alpha) cosd(theta)*sind(alpha) cosd(alpha) cosd(alpha)*d
             0 0 0 1];
    end


%01T
T01 = transform(theta1, 0);

%12T
T12 = transform(theta2, L1);

%23T
T23 = transform(theta3, L2);

%34T
T34 = transform(0, L3);

%Return
T02 = T01*T12;
T03 = T02*T23;
T04 = T03*T34;
end
