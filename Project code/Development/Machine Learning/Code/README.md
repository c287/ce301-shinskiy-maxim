### Code

Directory contains functions to run two experiments: [experiment.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/3d08f7b8ccefa0f10279d4e2e1757c21a2f0c6bb/Project%20code/Development/Machine%20Learning/Code/experiment.m) and [experimentRL.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/3d08f7b8ccefa0f10279d4e2e1757c21a2f0c6bb/Project%20code/Development/Machine%20Learning/Code/experimentRL.m). They were combined into one directory because they share functions.



##### Experiments

The target of these experiments is to provide a basic environment to combine with some learning/searching algorithm, that will come up optimal solution to the problem (dart throwing).

experiment.m is a first version of developed environment that has no searching algorithms implementations, meaning is does not find solution. It completes 10 throws and plots every throw to visualize it.

Robot manipulator is presented as a 3 degrees-of-freedom planar manipulator, with links 1, 2 and 3 of length 50, 40 and 15 in cm respectively (so it gets shorter as it gets further away from the base). And it is originated at `x = 500, y = -100` and the bullseye is at `(0, 0)`. As previously stated, function has no searching algorithm implementations yet and therefore after the throw, manipulator receives a reward for it but it does not affect it's behaviour. 

The resulting throw depends on the joint angles of the robot manipulator. These joint angles are generated randomly within certain discrete region. The limits are so: 
```math
90 < \theta_1 \le 180\\
 -165 \le \theta_2 < 0\\
 0 < \theta_3 \le 30\\
 50 < v_0 \le 150\tag{1}
 ```
where $`v_0`$ is initial velocity in cm/s given to a dart at release.

After 10 sets are generated, they are iterated through. The angles represent the joint angles of the manipulator at the moment of a throw, to draw this position angles are processed by [forward.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/f918069cd966988e432bf36bde24c7e2d941d525/Project%20code/Development/Machine%20Learning/Code/forward3DoF.m) function (refer [here](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/f918069cd966988e432bf36bde24c7e2d941d525/Project%20code/Development/3DoF/README.md) for details on the function) to find the coordinates of joints of the links. Then to find the trajectory call forcedTrajectory.m that calculates position of the dart through the trajectory at a certain rate. Last entry in the trajectory array will be where the dart has laded and this will be the starting point for the rewardFunction.m. Therefore the reward depends on the time travelled by the dart and a distance to the bullseye from a point where it landed.

To observe the throw, a position of the manipulator is plotted together with the animation of a dart throw.

experimentRL.m is a modified version of experiment.m. It has an implementation of a brute-force-like search algorithm, also it makes use of few introduced functions that improve the accuracy and bring conditions a step closer to real.

In this version, action space (set of chosen angles) is picked by a random choice from a pool of values and each variable has it's own pool, the action space is made of three initial angles and three velocities (one for every joint). Also, now, the launch angle is found on the trajectory of motion of end effector. It is assumed that the acceleration of manipulator's joints is instantaneous and speed is constant. Hence, the change in angle over time (angular speed) is constant and is implemented by increasing angle of the joint by a velocity constant after each iteration. Each iteration represents a time unit (a second) for which a manipulator moves for a bit, together, the for loop, creates a simulation of manipulator's trajectory, it then stores the orientation of last link (coordinates of both ends). The orientation is used in findSuitableAngle.m to decide the most suitable moment of the release along the trajectory, with a requirement that the dart is at 90 degrees to the link because it is to be assumed it held by the gripper on the end of the link as shown in Figure 1. Instead of randomizing the linear velocity of a dart, like in experiment.m, it is now calculated out of joint angles and their angular velocities in linearVelocity.m.

| ![Robot manipulator holding a dart](/doc%20src/robot_holding.png "Figure 1") |
| ------------------------------------------------------------ |
| Figure 1. Robot manipulator holding a dart                   |

Linear velocity of an end effector, that becomes an initial linear velocity of a dart, angle of release and coordinates of the end-effector at that moment are needed to calculate the trajectory of the dart thrown. After the trajectory is calculated in forcedTrajectory.m the reward is given by reward function, based on the landing position. The results together with the angle and velocity configuration are then tabulated. Configuration with the highest reward can be then found manually. If wished, every result can be plotted by visualize function, but can be commented out to save time.



##### Release Point Decision: [findSuitableAngle.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/121f95c6093f5f3cdb6b5b90e1e79f2418541717/Project%20code/Development/Machine%20Learning/Code/findSuitableAngle.m)

To find the optimal angle of release, this function is called with target coordinates and trajectory of $`L_3`$, the last link of the robot. Out of trajectory coordinates, function calculates aim angle, that is shown on figure 2 as a black line and a yellow semicircle attached to it to show the angle. It is a the direction at which the dart will be thrown, it is assumed that the dart is located right at the end of the link 3 as a tangent to it and therefore the calculation is based around that assumption. Another angle is represented as a line from the tip of the link 3 to the target (bullseye) and is shown in pink semicircle.

| ![Robot manipulator throwing a dart](/doc%20src/robot_throwing.png "Figure 2") |
| ------------------------------------------------------------ |
| Figure 2. Robot manipulator throwing a dart                  |

To choose the angle it is needed to find a difference in the base and aim angles and the most optimal angle will be the one that is closest to the base angle. This works in relatively short distances. Due to gravity, the dart will fall and larger distances the dart will fall more, increasing the error.



##### Trajectory of a Dart: [forcedTrajectory.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/121f95c6093f5f3cdb6b5b90e1e79f2418541717/Project%20code/Development/Machine%20Learning/Code/forcedTrajectory.m)

This is a modified version of a previously developed [calculation](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/121f95c6093f5f3cdb6b5b90e1e79f2418541717/Project%20code/Development/DartTrajectory/calculation.m) function. It is modified in a way that it doesn't need to calculate the angle anymore because it is already calculated in the findSuitableAngle function. Also it calculates time for accurate with a introduced time equation:
```math
y-h = vtsin(\theta) - \frac{gt^2}{2}\tag{2}
```
 

largest root of this quadratic tells the time of flight of the projectile from a point of release till it lands at the target or goes below the certain height $`h`$. The trajectory is calculated at a rate of 500 coordinate points per trajectory.



##### Forward Function: [forward3DoF.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/121f95c6093f5f3cdb6b5b90e1e79f2418541717/Project%20code/Development/DartTrajectory/calculation.m)

Function was left unchanged since development. Description of it can be found [here](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Development/3DoF).



##### Angular to Linear: [linearVelocity.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Project%20code/Development/3DoF)

This is a simple function to calculate linear velocity at the tip of end effector produced by angular velocity.
```math
v_x = -(L_1\omega_1sin(\theta_1) + L_2\omega_2sin(\theta_1+\theta_2) + L_3\omega_3sin(\theta_1+\theta_2+\theta_3))\\
v_y = L_1\omega_1cos(\theta_1) + L_2\omega_2cos(\theta_1+\theta_2) + L_3\omega_3cos(\theta_1+\theta_2+\theta_3)\\
v = \sqrt{v_x^2 + v_y^2}\tag{3}
```


##### Reward Function: [rewardFunction.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/121f95c6093f5f3cdb6b5b90e1e79f2418541717/Project%20code/Development/Machine%20Learning/Code/rewardFunction.m)

Reward function takes the distance to the bullseye from the landing point and a time of flight. The idea is that the reward must represent the ratio of accuracy and steepness of the trajectory. It was used in experimentRL.m, however, further in the project it was deprecated.



##### Experiment Visualization: [visualize.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/121f95c6093f5f3cdb6b5b90e1e79f2418541717/Project%20code/Development/Machine%20Learning/Code/visualize.m)

To minimize the time taken for an experiment, the plotting part was made optional and put into separate function. It's not much different from the plotting part in experiment.m. It plots every position of the manipulator's trajectory until the position of a throw which is indicated by `index` variable. Then it plots  the trajectory of a dart.

