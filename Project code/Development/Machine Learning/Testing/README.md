### Testing

Current directory presents the results of running the experimentRL.m file.

As a result of one run a table result_table_1000x7.mat was manually saved. It contains log of that run with configurations and a rewards was corresponding configuration. The [table](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/4ef66966123a898cf5f15d2e7c0d8aaa088d5a21/Project%20code/Development/Machine%20Learning/Testing/result_table_1000x7.mat) is in a following form:

|$`\theta_1`$|$`\theta_2`$|$`\theta_3`$|$`\omega_1`$|$`\omega_2`$|$`\omega_3`$|reward|
| --------- | --------- | --------- | --------- | --------- | --------- | --------- |
| 105 | -150 | -90 | 6 | 2 | 5 | 8.1824e+03 |
| ... | ... | ... | ... | ... | ... | ... |

The best result was found in the table and [visualized](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/4ef66966123a898cf5f15d2e7c0d8aaa088d5a21/Project%20code/Development/Machine%20Learning/Testing/best_result.fig) (figure 1).

| ![Best Result](/doc%20src/best_result.png "Figure 1") |
| :----------------------------------------------------------: |
|                    Figure 1. Best result                     |

