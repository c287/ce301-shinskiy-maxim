function [suitAngle, index] = findAngle(targetX, targetY, effPos)
%FINDANGLE - find the angle that is closer to 45 deg

    %preallocation
    s = size(effPos);
    releaseAngle = zeros(1,s(1));

    %iterate over a list of eff
    for i = 2:size(effPos, 1)

        %current aim angle in rads
        aimAngle = atan2(effPos(i, 2) - effPos(i-1, 2), effPos(i, 1) - effPos(i-1, 1));


        if(aimAngle < 0)
            aimAngle = aimAngle + 2*pi;
        end

        %base angle in rads
        baseAngle = atan2(targetY-effPos(i-1, 2), targetX-effPos(i-1, 1));

        if(baseAngle < 0)
            baseAngle = baseAngle + 2*pi;
        end

        %realese_angle = difference in base and aim
        %convert to degrees for output
        releaseAngle(i - 1) = rad2deg(baseAngle - aimAngle);
    end

    [~, index] = min(abs(releaseAngle(releaseAngle > 0) - 45));
    suitAngle = releaseAngle(index);
end

