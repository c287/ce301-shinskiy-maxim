%Program to generate an expirement with random state.

%Limbs
L1 = 50;
L2 = 40;
L3 = 15;

%Board coord for plot
board = [0 -25; 0 0; 0 25];
be = [0 -2; 0 0; 0 2]; %BullsEye

%Generate random state
for i = 1:5
    %[theta1, theta2, theta3, initial velocity] 
    % 90-180, 0-(-165), 0-30, 5-15 <--Ranges
    % pick reasonable numbers to include joint limits and possible dart speed
    % in degress and m/s respectively
    randState(i, :) = [90 + randi(90),  -randi(165), randi(30), 5 + randi(10)]; 
end


for rs = 1:length(randState)
    close all
    
    %Message
    disp(['Experiment: ', num2str(rs)])
    disp([randState(rs, 1), randState(rs, 2), randState(rs, 3), randState(rs, 4)])
    disp("----------------------------");
    
    %function calls
        %calculate manipulators coords
    [T02, T03, T04] = forward3DoF(L1, L2, L3, randState(rs, 1), randState(rs, 2), randState(rs, 3));
        %Coordiante of a manipulator
    limbs = [[[[500 0 ]; T02(1:2, 4)']; T03(1:2, 4)']; T04(1:2, 4)'];
        %calculate trajectory
    [traj, t] = forcedTrajectory(randState(rs, 4), randState(rs, 4) + 90, limbs(4, 1), limbs(4, 2)); 
   
    
    figure(1);
    
    plot(board(:,1), board(:, 2), "g-", "LineWidth", 2); %board
    hold on
    plot(be(:, 1), be(:, 2), "r-", "LineWidth", 3); %bullseye
    hold on
    plot(limbs(:, 1), limbs(:, 2), 'bo-'); %manipulator
    hold on
    axis([-100 900 -500 500]);
    
    %pause;
    %plot trajectory
    for time = 1:size(traj, 1)
        plot(traj(1:time, 1), traj(1:time, 2), "b.-"); %projcetile
        hold on
       
        pause(0.1);
    end
    
    %wait for 10 seconds
    pause(3)
end