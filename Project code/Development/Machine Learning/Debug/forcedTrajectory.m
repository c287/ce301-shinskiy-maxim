function [traj, time] = forcedTrajectory(velocity, angle, x, y)
%FORCEDTRAJECTORY calculates the trajectory of a thrown dart until x of
%dart < 0
    traj = [];

    g = 9.81; %gravity pull
    %translate to (0, 0) base system
    x = -x;
    y = -y;
    y0 = -y; %height y0 with respect to the point of impact

    v = velocity;
    
    t = (v*sind(angle) + sqrt((v*sind(angle))^2 + 2*g*y0))/g; %time of flight
        
    if t > 20
       error("Time of flight > 20s")
    end
    
    for time = 0:0.05:t
        
        posX = v*time*cosd(angle) - x;%X at time T
        posY = v*time*sind(angle) - (g*time^2)/2 - y;%Y at time T
        if(posX > 0)
            traj = [traj; [posX posY]];       
        else 
            break;
        end
    end
    
end