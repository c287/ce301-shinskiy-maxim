function [T02, T03, T04] = forward3DoF(L1, L2, L3, theta1, theta2, theta3)
%FORWARD3DOF function to calculate coords of a manipulator
%   This function calculates the points of each joint and end effector
%   so they can be then plotted

%Parameters for 01T
theta = theta1;
alpha = 0;
d = 0;
a = 0;

%General transform for i-1 i T
T = [cosd(theta) -sind(theta) 0 a
    sind(theta)*cosd(alpha) cosd(theta)*cosd(alpha) -sind(alpha) -sind(alpha)*d
    sind(theta)*sind(alpha) cosd(theta)*sind(alpha) cosd(alpha) cosd(alpha)*d
    0 0 0 1];

T01 = [cosd(theta) -sind(theta) 0 a
    sind(theta)*cosd(alpha) cosd(theta)*cosd(alpha) -sind(alpha) -sind(alpha)*d
    sind(theta)*sind(alpha) cosd(theta)*sind(alpha) cosd(alpha) cosd(alpha)*d
    0 0 0 1];

%Parameters for 12T
theta = theta2;
a = L1;


T12 = [cosd(theta) -sind(theta) 0 a
    sind(theta)*cosd(alpha) cosd(theta)*cosd(alpha) -sind(alpha) -sind(alpha)*d
    sind(theta)*sind(alpha) cosd(theta)*sind(alpha) cosd(alpha) cosd(alpha)*d
    0 0 0 1];

%Parameters for 23T
theta = theta3;
a = L2;

T23 = [cosd(theta) -sind(theta) 0 a
    sind(theta)*cosd(alpha) cosd(theta)*cosd(alpha) -sind(alpha) -sind(alpha)*d
    sind(theta)*sind(alpha) cosd(theta)*sind(alpha) cosd(alpha) cosd(alpha)*d
    0 0 0 1];

%Parameters for 34T
theta = 0;
a = L3;

T34 = [cosd(theta) -sind(theta) 0 a
    sind(theta)*cosd(alpha) cosd(theta)*cosd(alpha) -sind(alpha) -sind(alpha)*d
    sind(theta)*sind(alpha) cosd(theta)*sind(alpha) cosd(alpha) cosd(alpha)*d
    0 0 0 1];

%Return
T02 = T01*T12;
T03 = T02*T23;
T04 = T03*T34;
end

