%Program to generate an expirement with discrete state.

%Calculate every position of eef for S seconds
%find the angle closest to optimal (45 deg)
close all

%run time
tic

%for loops
N = 1;
S = 20;

%end-effector position
effPos = zeros(S, 2);

%Trajectories and Rewards for them
log = zeros(S, 3);
trajRewards = zeros(1, S);

%table
table = zeros(N, 7);

%Links
L1 = 50;
L2 = 40;
L3 = 15;

%Origin of manipulator
origin = [500 -90];

%Board coord for plot
board = [0 -25; 0 0; 0 25];
be = [0 -2; 0 0; 0 2]; %BullsEye

%Velocities choice
vels = [4 5 6 7 8 9 10 11 12
        4 5 6 7 8 9 10 11 12
        4 5 6 7 8 9 10 11 12];

%Angles choice
angles = [75 80 85 90 95 100 105
        -120 -125 -130 -135 -140 -145 -150
        -75 -80 -85 -90 -95 -100 -105];

%repeat experiment N times
for n = 1:N
    
    %Pick angles and velocities to stick with
    angleChoice = [angles(1, randi(7)); angles(2, randi(7)); angles(3, randi(7))];
    velChoice = [vels(1, randi(9)); vels(2, randi(9)); vels(3, randi(9))];  
    
    angle1 = angleChoice(1);
    angle2 = angleChoice(2);
    angle3 = angleChoice(3);
    
    vel1 = velChoice(1);
    vel2 = velChoice(2);
    vel3 = velChoice(3);

    %for 30 seconds (steps)
    for step = 1:S
        %functions calls
        %calculate manipulators coords
        [T02, T03, T04] = forward3DoF(L1, L2, L3, angle1, angle2, angle3);
        links = [[[origin; T02(1:2, 4)' + origin]; T03(1:2, 4)' + origin]; T04(1:2, 4)' + origin];
        
        %Store end-effector position
        eff = T04(1:2, 4)' + origin;
        effPos(step, :) = eff;
        
        %Update angles
        angle1 = angle1 + vel1;
        angle2 = angle2 + vel2;
        angle3 = angle3 + vel3;
    end
    
    %find angle that is closer to 45deg from end-effectors position
    [suitAngle, index] = findAngle(board(2, 1), board(2, 2), effPos);
    
    %calculate trajectory and optimal angle
    [traj, t] = forcedTrajectory(vel3*10, suitAngle, effPos(index, 1), effPos(index, 2));
        
    %calculate distance to bullseye for reward function
    d = sqrt(traj(end, 1)^2 + traj(end, 2)^2);
    
    %reward for the trajectory
    reward = rewardFunction(d, t);
    
    %store sets(angle, vel, reward)
    angleSet = [angleChoice(1), angleChoice(2), angleChoice(3)];
    velocitySet = [velChoice(1), velChoice(2), velChoice(3)];
    
    table(N, :) = [angleSet, velocitySet, reward];

    %print message
    fprintf('Reward = %5.2f, time: %3.1f, dist: % 4.1f', reward, t, d);
    fprintf(newline);
    
    angle1 = angleChoice(1);
    angle2 = angleChoice(2);
    angle3 = angleChoice(3);
    
    vel1 = velChoice(1);
    vel2 = velChoice(2);
    vel3 = velChoice(3);
    
    %plot trajectory of manipulator and ~45deg dart trajectory
    for pl = 1:index         
        %function calls
        %calculate manipulators coords
        [T02, T03, T04] = forward3DoF(L1, L2, L3, angle1, angle2, angle3);
        %Coordinates of a manipulator
        links = [[[origin; T02(1:2, 4)' + origin]; T03(1:2, 4)' + origin]; T04(1:2, 4)' + origin];
        %Store end-effector position
        eff = T04(1:2, 4)' + origin;
        effPos(pl, :) = eff;

        figure(1);

        plot(board(:,1), board(:, 2), "k-", "LineWidth", 2); %board
        hold on
        plot(be(:, 1), be(:, 2), "r-", "LineWidth", 3); %bullseye
        hold on
        plot(links(:, 1), links(:, 2), 'bo-'); %manipulator
        
        if(pl < index)
           hold off 
        else 
           hold on
        end

        %plot(effPos(:, 1), effPos(:, 2), 'k.-'); %end-effector
        %hold off
        
        axis([-100 600 -350 350]);
        
        pause(0.01);
        
        %Update angles
        angle1 = angle1 + vel1;
        angle2 = angle2 + vel2;
        angle3 = angle3 + vel3;
        
    end


    %plot trajectory
    for time = 1:size(traj, 1)
        plot(traj(1:time, 1), traj(1:time, 2), "r-"); %projeсtile
        hold on
        pause(0.01);
        
        axis([-100 600 -350 350]);
    end
    toc
    
    
    pause(0.1);
end