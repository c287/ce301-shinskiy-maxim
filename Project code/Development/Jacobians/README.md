### Jacobians

This directory contains functions to transform angular velocity to linear and linear to angular. 

| ![Velocity vectors of neighboring links](/doc%20src/velocity_vectors.png "Figure 1") |
| ------------------------------------------------------------ |
| Figure 1. Velocity vectors of neighbouring links             |

Figure is adapted from: Craig, 1955. (1986). *Introduction to robotics : mechanics & control / John J. Craig.*. Reading, Mass.: Addison-Wesley Pub. Co.,. ISBN: 0201103265

##### Linear to Angular: [InverseVelocity.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/d2971e851e228e54703bafdaf3c627359ed3a33d/Project%20code/Development/Jacobians/InverseVelocity.m)

The equation for linear velocity with relation to angular velocity is:
```math
^{i+1}v_{i+1} =^{i+1}_{i}R(^{i}v_{i} + ^{i}\omega_{i} \times ^{i}P_{i+1})\tag{1}
```
where $`v`$ is a linear velocity, $`\omega`$ is angular velocity, $`R`$ is a rotational matrix, $`P`$ is a link vector (link length) and is constant.

Applying this successively, link to link, we result in  the linear velocity of a link in terms of frame {i+1}. However this is not quite useful and to find the angular velocity at the last link, it has to be multiplied by $`^{0}_{N}R`$ , where $`N`$ is the total number of frames (including end effector), in the case of this project is it $`N = 4`$:
```math
\^{0}_{4}v = ^{0}_{4}R * ^{4}_{4}v\tag{2}
```
The general form of a rotation matrix is:
```math
^{i}_{i+1}R = \begin{bmatrix}
cos(\theta_i) & -sin(\theta_i) & 0\\
sin(\theta_i) & cos(\theta_i) & 0\\
0 & 0 & 1
\end{bmatrix}
\tag{3}
```
also, 
```math
^{i+1}_{i}R = ^{i}_{i+1}R^{-1}\tag{4}
```
To compute angular velocity three syms values are introduced, then later in the code, a pair of simultaneous equations can be made from $`^{0}_{4}v`$ vector. This is 3 by 1 velocity vector that is made of 3 velocity components $`\begin{bmatrix} x\\ y\\ z \end{bmatrix}`$ . Z-component of the velocity will always be 0 due to the robot to be planar. Hence, $`v_x = x`$ and $`v_y = y`$, $`x`$ and $`y`$ will be equations involving angular velocities for every three frames, $`v_x`$ and $'v_y'$ are horizontal and vertical components of linear velocity respectively, it is supplied as an argument to this function. The actual calculation is left for the MatLab to solve via `solve()` function.



##### Angular to Linear: [forwardVelocity.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/d2971e851e228e54703bafdaf3c627359ed3a33d/Project%20code/Development/Jacobians/forwardVelocity.m)

In forward function it is required to find the linear velocity from angular. This process is very similar to inverse function, in fact, it is the same but few steps shorter. Now, angular properties are supplied as an argument and therefore all is left to do is put them into the formula. 

The process starts the same as in inverse function: find rotational matrix $`^{i}_{i+1}R`$ for every frame {i} with the same matrix $`(3)`$, then find every  $`^{i+1}_{i+1}v`$. Then $`v_x`$ is a top row of $`^0_4R * ^4_4v`$ vector and $`v_y`$ is a middle row.
