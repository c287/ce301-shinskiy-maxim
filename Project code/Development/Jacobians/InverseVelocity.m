function [omega1, omega2, omega3] = InverseVelocity(L1, L2, L3, O1, O2, O3, Vx, Vy)
%Calculate angular velocity for given 3dof setup
%And given linear velocity
%Alternatively instead of Vx and Vy, |V| and theta constuctor possible
%Calculates thetas in DEGREES
%This function is NOT generalized and for specific purpose ONLY


%Calculate rotational matrices, and find R04

R12 = [cosd(O1) -sind(O1) 0
    sind(O1) cosd(O1) 0
    0 0 1];

R23 = [cosd(O2) -sind(O2) 0
    sind(O2) cosd(O2) 0
    0 0 1];

R34 = [cosd(O3) -sind(O3) 0
    sind(O3) cosd(O3) 0
    0 0 1];

R04 = R12 * R23 * R34;

%Calculate V44
%v0 = 0, v1 = 0, start from v2
syms w1 w2 w3;

v2 = (inv(R12)) * ([0; w1*L1; 0]);

v3 = (inv(R23)) * (v2 + [0; w2*L2; 0]);

v4 = (inv(R34)) * (v3 + [0; w3*L3; 0]);

%Calculate V04
v04 = R04 * v4;

eqX = v04(1) == Vx;
eqY = v04(2) == Vy;

[omega1, omega2, omega3] = solve(eqX, eqY, w1, w2, w3)
end
