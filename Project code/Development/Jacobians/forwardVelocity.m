function [effV] = forwardVelocity(L1, L2, L3, O1, O2, O3, w1, w2, w3)
%FORWARDVELOCITY compute the linear velocity
%using the angular velocity and link length

    %Calculate rotational matrices, and find R04
    function R = rotMatrix(theta)
        cosdT = cosd(theta);
        sindT = sind(theta);
        
        R = [cosdT -sindT 0
            sindT cosdT 0
            0 0 1];
    end

    R12 = rotMatrix(O1);
    R23 = rotMatrix(O2);
    R34 = rotMatrix(O3);

    R04 = R12 * R23 * R34;

    %Calculate V44
    %v0 = 0, v1 = 0, start from v2
                    %syms w1 w2 w3;

    v2 = (inv(R12)) * ([0; w1*L1; 0]);

    v3 = (inv(R23)) * (v2 + [0; w2*L2; 0]);

    v4 = (inv(R34)) * (v3 + [0; w3*L3; 0]);

    %Calculate V04
    effV = R04 * v4;
end