function [angle, trajectory, t] = calculation(velocity, x, y)
    trajectory = [];
    %translate to (0,0) base system
    x = -x; 
    y = -y;
    y0 = -y; %height y0 with respect to the point of impact

    
    v = velocity
    g =  9.81; %gravity pull
    
    %find angle of release
    angle1 = atand(((v^2) + sqrt((v^4) - g*(g*(x^2)+2*y*(v^2))))/(g*x)); %(+ve)
    angle2 = atand(((v^2) - sqrt((v^4) - g*(g*(x^2)+2*y*(v^2))))/(g*x)); %(-ve)
    %choose minimum angle for minimum trajectory length
    angle = minAngle(angle1, angle2) + 180; %needs 180 to traslate back from (0,0) system
    
    %return error message if velocity is to little for specified distance
    %maxD = ((v*cosd(angle))/g)*(v*sind(angle) + sqrt((v^2)*(sind(angle)^2) + 2*g*y0))
    %if(abs(v) <= sqrt(x^2 + y^2))
    %    error("Out of Range!");
    %end
    
    t = (v*sind(angle) + sqrt((v*sind(angle))^2 + 2*g*y0))/g; %time of flight
    
    disp("Possible angles:")
    disp(['angle1 = ', num2str(angle1), ', angle2 = ', num2str(angle2)])
    disp(['Optimal = ', (num2str(angle-180))])
    disp(['Time of travel = ', num2str(t)])
    
    
    for time = 0:t/20:t
        
        posX = v*time*cosd(angle) - x;%X at time T
        posY = v*time*sind(angle) - (g*time^2)/2 - y;%Y at time T
        if(posX > 0)
            trajectory = [trajectory; [posX posY]];
        end
    end
end
