### Dart Trajectory 

To throw a dart in the simulation in a final product, this code was developed. It calculates the trajectory of a flight of a dart. It is assumed that the only force acting on the dart is gravitational pull, resistance/drag is negligible and acceleration is instantaneous.



##### Trajectory Calculation: [calculation.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/d89fc37e13d7f00e2210bc315f35479eee919f3b/Project%20code/Development/DartTrajectory/calculation.m)

As a convention, in this project origin (0, 0) is taken at the base of the robot, however for the projectile motion equation it is required the that the origin is at the target. Therefore the coordinate system was translated by swapping $`x`$ and $`y`$: `x = -x; y = -y;`.

At first it is required to find the launch angle, for a trajectory that hits the bullseye:
```math
\theta_{1,2} = arctan(\frac{v^2 \pm \sqrt{v^4 - g(gx^2 + 2yv^2)}}{gx})
```
Two launch angles means two possible trajectory. The desired trajectory will be the one at which the dart will travel less, therefore the launch angle must be acute and the path - steep. Choosing the angle closer to 0 will guarantee that the chosen trajectory is steeper than rejected trajectory. The choice of an angle happens in the `minAngle()` function. 

It is useful to know when the dart hits the dart and to calculate that the time of flight equation is used:
```math
t_{flight} = \frac{vsin(\theta) + \sqrt{vsin(\theta)^2 + 2gh}}{g}
```
To store the trajectory data ($`x`$ and $`y`$ position of a projectile), the for loop is used with the projectile equations:
```math
x_t = vtcos(\theta) - x\\
y_t = vtsin(\theta) - \frac{gt^2}{2} - y
```
 The trajectory is stored as long as trajectory not beyond the target along the x-axis.



##### Additional function: [minAngle.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/d89fc37e13d7f00e2210bc315f35479eee919f3b/Project%20code/Development/DartTrajectory/minAngle.m)

Just standard MatLab `min()` is not suitable simply because it finds the value closer to $`-Inf`$ rather than $`0`$, instead an additional functionality was introduced. After both values had absolute operation done upon them and then compared, the value is matched to it's initial magnitude and then returned. This way `minAngle()` function undoes the absolute operation that was done at the step of comparison. 

e.g.


```matlab
a = minAngle(-43, 55)
b = min(-55, 43)
c = min(abs(-43), abs(55))
```

then:

```matlab
a = -43	%Correct
b = -55	%Wrong
c = 43  %Wrong
```

##### Trajectory plotting: [trajectory.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/d89fc37e13d7f00e2210bc315f35479eee919f3b/Project%20code/Development/DartTrajectory/trajectory.m)

This function visualises the dart motion. It calls `calculation()` with some arbitrary arguments to get trajectory matrix. To plot the trajectory it iterates through the trajectory matrix and plots position of the dart on every iteration.



|![Resulting dart trajectory](/doc%20src/dart_trajectory_result.png "Figure 1.")|
|---------|
|Figure 1. Resulting dart trajectory|
