function [minimum] = minAngle(o1, o2)
    %This function takes two arguments
    %and returns a value that is closer to 0

    angle = min(abs(o1), abs(o2));

    if(angle == abs(o1))
        minimum = o1;
    elseif(angle == abs(o2))
        minimum = o2;
    end
end