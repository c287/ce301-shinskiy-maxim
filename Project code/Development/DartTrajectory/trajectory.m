%Program to calculate the trajectory of a dart and plot a graph
close all

disp("Trajectory")

[theta, traj, t] = calculation(10, 7, -2);
board = [0 -1; 0 0; 0 1];
be = [0 -.2; 0 0; 0 .2];

figure(1);
plot(board(:,1), board(:, 2), "k-", "LineWidth", 2); %board
hold on
plot(be(:, 1), be(:, 2), "r-", "LineWidth", 2); %bullseye
hold on

for time = 1:1:size(traj, 1)
    plot(traj(1:time, 1), traj(1:time, 2), "b.-"); %trajectory
    hold on
    
    axis([-2 8 -5 5]);
    pause(0.1);
end
