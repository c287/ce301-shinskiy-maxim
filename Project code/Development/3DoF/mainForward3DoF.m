close all;
disp("forward 3 degree of freedom kinematics")

L1 = 4;
L2 = 3;
L3 = 2;

o1 = 90;
o2 = 90;
o3 = 90;

theta1 = [0:o1/100:o1];
theta2 = [0:o2/100:o2];
theta3 = [0:o3/100:o3];

angleVar = [];
%angleVar(theta1(:, 1), 1) = theta1(:, 2);
%angleVar(theta2(:, 1), 2) = theta2(:, 2);
%angleVar(theta3(:, 1), 3) = theta3(:, 2);
for i = 1:length(theta1)
    angleVar(i, :) = [theta1(i), theta2(i), theta3(i)];
end
eep = [];


for av = 1:length(angleVar) 
    %angleVar(av,2)
    %jointVariations(av,3)
    [T02, T03, T04] = forward3DoF(L1, L2, L3, angleVar(av, 1), angleVar(av, 2), angleVar(av, 3))
    limb1 = [[0 0 ];T02(1:2, 4)'];
    limb2 = [limb1; T03(1:2, 4)'];
    limb3 = [limb2; T04(1:2, 4)'];
    
    eep = [eep; limb3(end,:)];
    figure(1);
    plot(limb3(:,1), limb3(:,2), 'bo-');
     hold on;
     plot(eep(:,1), eep(:,2), 'g*-');
%     hold on;
%     plot(limb3(1,:), limb(2,:), 'bo-')
%     hold on
    %plot(eep(:,1), eep(:,2), 'r*-')
    hold off;
    
    axis([-10 10 -10 10]);
    axis square;
    pause(0.01);
end