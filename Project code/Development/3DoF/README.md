### Forward Kinematics for 3DoF manipulator

This directory contains code developed for forward kinematics of 3 degree-of-freedom robot manipulator. 

##### Forward kinematics function: [forward3DoF.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/ddf0b01b1933f3396264b985d7ec8ac0b6c98ee7/Project%20code/Development/3DoF/forward3DoF.m)

Function makes use of the Denavit-Hartenberg parameters to calculate frame parameters with respect to the base frame as can be seen in Figure 1. First step in calculation will be assigning link frames to the mechanism. The "mechanism" itself is an argument to this function: $`L1`$, $`L2`$, $`L3`$. It is assumed that the robot is a 3 degree-of-freedom planar arm manipulator. Then combine Figure 1 and Figure 2 into a link parameters table, Table 1.

|![Link-frame assignments](/doc%20src/3dof_link_frame_assignment.png "Figure 1") |![planar arm](/doc%20src/3link_planar_arm.png "Figure 2")
|------------------|------------------|
|Figure 1. Link-frame assignment|​Figure 2. Planar arm|





| $`i`$ | $`\alpha_{i-1}`$ | $`a_{i-1}`$ | $`d_i`$ | $`\theta_i`$ |
| ----- | ---------------- | ----------- | ------- | ------------ |
| 1     | 0                | 0           | 0       | $`\theta_1`$ |
| 2     | 0                | $`L_1`$     | 0       | $`\theta_2`$ |
| 3     | 0                | $`L_2`$     | 0       | $`\theta_3`$ |

Table 1.  Link parameters table

After creating a table next step is to interpret it into the DH matrix, whose standard form is:
```math
T^{i-1}_{i} = \begin{vmatrix}
c\theta & -s\theta_i & 0 & a_{i-1} \\ 
s\theta_{i}c\alpha_{i-1} & c\theta_{i}c\alpha_{i-1} & -s\alpha_{i-1} & -s\alpha_{i-1}d_i \\
s\theta_{i}s\alpha_{i-1} & c\theta_{i}s\alpha_{i-1} & c\alpha_{i-1} & c\alpha_{i-1}d_i \\
0 & 0 & 0 & 1
\end{vmatrix}
```
$`T^{i-1}_i`$ means "transformation matrix of frame $`i`$ with respect to the previous frame $`i-1`$. In this matrix $`c`$ is short for *cosine*,$`s`$ is short for *sine*, $`a`$ is a length of a common normal (link length), $`d`$ is an offset along previous link to the common normal, $`\theta`$ is an angle about $`X`$ axis from previous to current frame,  $`\alpha`$ is a twist  (rotation about $`Z`$) because of the assumption that the manipulator is planar, hence it has no $`Z`$ plane and $`\alpha`$ is always 0.

This part is implemented as a sub-function `transform(theta, a)` that takes current link's length and angle to the previous frame. Computed matrices will be with respect to the previous frame, to find the coordinates of the end effector with respect to the base (origin), it is required to complete another series of calculations:
```math
T^0_2 = T^0_1*T^1_2\\
T^0_3 = T^0_2 * T^2_3\\
T^0_4 = T^0_3 * T^3_4 
```
The result matrix: $`T^0_4`$ contains cartesian coordinates of end effector.
```math
x = T^0_4(1, 4)' + x_{origin}\\
y = T^0_4(2, 4)' + y_{origin}
```
Figure 1 and 2 were adapted from [1].

##### Main Function: [mainForward3DoF.m](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/ddf0b01b1933f3396264b985d7ec8ac0b6c98ee7/Project%20code/Development/3DoF/mainForward3DoF.m)

This is a piece of code to visually demonstrate the functionality of forward kinematics function. $`L1`$, $`L2`$ and $`L3`$ specify desired link lengths, $`o1`$, $`o2`$ and $`o3`$ is the set of angle at final position. In order to create an animation of motion, for loop includes a `plot()` function to plot the orientation of robot manipulator at every step, of which there are 100. In figure 3, manipulator is shown in blue with joints circled, the trajectory of end effector in shown in green.

|![Resulting Orientation](/doc%20src/result_figure.png "Figure 3")|
|-----------|
|Figure 3. Resulting Orientation|

[1]: Craig, 1955. (1986). *Introduction to robotics : mechanics & control / John J. Craig.*. Reading, Mass.: Addison-Wesley Pub. Co.,. ISBN: 0201103265

