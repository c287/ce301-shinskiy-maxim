### Background Reading Summary

This directory contains the literature that was reviewed in the process of preparation to the Capstone Project and was a part of a task for a Challenge Week.

The directory contains several PDFs, which are the articles and/or books about the similar to the Dart Throwing Robot project's topic. Also, it contains a text file that has links to the videos. These videos show the nature of dart throwing and material that might be useful in the project development. 

This literature is summarized in the [word file](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/0e595c959557d577d9e0b7d733e1e3bc37981bda/Challenge%20Week/Background%20Literature/Maxim_Shinskiy_Background_Reading_Summary.docx).