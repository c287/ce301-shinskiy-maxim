close all;

effPos = endeffectorPosition;
disp("inverse kinematics")
for ep = 1:length(effPos)     
    angles = inverse(effPos(ep,2), effPos(ep, 3));
    joint1 = angles(1);
    joint2 = angles(2);
    effPos(ep, 2) = joint1;
    effPos(ep, 3) = joint2;
end
    plot(effPos(:, 1), effPos(:, 2), '-r*')
    hold on;
    plot(effPos(:, 1), effPos(:, 3), '-bo')
    hold on;
        
    legend('joint1', 'joint2')
    
