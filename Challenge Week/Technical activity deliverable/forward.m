
function [coordinates] = forward( angle1, angle2)
    %FORWARD find the forward kinematics
    base = [0 0];

    L1 = 5;
    L2 = 5;
    
    %Eff. position 1
    eff1x = L1*cosd(angle1);
    eff1y = L1*sind(angle1);
                
    %Eff. position 2
    eff2x = eff1x + L2*cosd(angle1 + angle2);
    eff2y = eff1y + L2*sind(angle1 + angle2);
            
    coordinates = [0 0; eff1x eff1y; eff2x eff2y];
end