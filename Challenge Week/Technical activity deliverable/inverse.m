function [angles] = inverse(x, y)
%INVERSE find inverse kinematics
    L1 = 5;
    L2 = 5;

    if sqrt(x^2 + y^2) > L1 + L2
        disp("Out of work space");
        return
    end
       
    angle2 = acosd((x^2 + y^2 - L1^2 - L2^2)/(2*L1*L2));
    angle1 = atand(y/x) - atand((L2*sind(angle2))/(L1 + L2*cosd(angle2)));

    angles = [angle1, angle2];
end

