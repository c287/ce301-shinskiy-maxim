### Technical Activity Deliverable

This directory contains code part of Challenge Week deliverables. 

The task is described in [Task directory](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Challenge%20Week/Technical%20activity%20deliverable/Task).

##### Task 1: Forward Kinematics

Forward kinematics is a computation of a position of end-effector from known joint parameters, by using kinematics equations, in this case trigonometry:

```math
x = L_1 cos(\theta_1) + L_2 cos(\theta_1 + \theta_2)
\\
y = L_1 sin(\theta_1) + L_2 sin(\theta_1 + \theta_2)
```

where $`L_1`$ is a length of first link and $`L_2`$ is a length of seconds link, $`\theta_1`$ is an angle between base (ground) and first link and $`\theta_2`$ is an angle between first and second links.

Forward kinematics code is made out of two parts: [function](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/4c8d8130b90009c53d2df5e6bec2a08b58d7c7e7/Challenge%20Week/Technical%20activity%20deliverable/forward.m) and [main part](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/4c8d8130b90009c53d2df5e6bec2a08b58d7c7e7/Challenge%20Week/Technical%20activity%20deliverable/mainForward.m).

Main part is a for loop that reads the data from [matrix](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/4c8d8130b90009c53d2df5e6bec2a08b58d7c7e7/Challenge%20Week/Technical%20activity%20deliverable/Task/jointVariations.mat) supplied with the task and plots the 2DoF robot out of it. Matrix is the list of joint angles of the robot. After one set of angles are read, main program then calls forward function that calculates the coordinates of manipulator's links for this set of angles, then main program takes these coordinates to plot the manipulator. Plotting the manipulator for one joint angle set at a time creates an animation.



##### Task 2: Inverse Kinematics

Inverse kinematics requires opposite to forward: calculating joint parameters from known end effector position. In case with 2 degree-of-freedom robot manipulator, this can be as well done by using trigonometry. However, the answer to this problem is not that trivial. The end effector position can be reached by 2 ways (e.g. elbow up/down). The choice of which way to use is based on other conditions obstacle around the manipulator.
```math
\theta_1 = arctan(\frac{y}{x}) - arctan(\frac{L_2sin(\theta_2)}{L_1 + L_2cos(\theta_2)})
\\
\theta_2 = arccos(\frac{x^2 + y^2 - (L_1^2 + L_2^2)}{2L_1L_2})
\\or\\
\theta_2 = \pi - arccos(\frac{L_1^2 + L_2^2 - (x^2 + y^2)}{2L_1L_2})
```
As well as in Forward Kinematics, here $`\theta_1`$ and $`\theta_2`$ are angles between base and link 1, and link 1 and link 2 respectively, $`L_1`$ and $`L_2`$ are link lengths of link 1 and link 2 respectively. 

Inverse Kinematics code is also made out of two parts: [function](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/4c8d8130b90009c53d2df5e6bec2a08b58d7c7e7/Challenge%20Week/Technical%20activity%20deliverable/inverse.m) and [main part](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/4c8d8130b90009c53d2df5e6bec2a08b58d7c7e7/Challenge%20Week/Technical%20activity%20deliverable/mainInverse.m).



Main function is made according to the task, it stores angles that were calculated by inverse function and plots a graph of two angles. inverse.m calculates set of joint angles from supplied $`x`$ and $`y`$ positions of end effector and returns the set. It uses equations above to calculate the angles, for $`\theta_2`$ it uses an equation without $`\pi`$ part.
