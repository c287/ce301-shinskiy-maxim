### Task

Technical activity was provided by the supervisor in the beginning of the Challenge Week and was set due to the end of it (5 days). Activity is made of two tasks: forward and inverse kinematics.

##### Forward kinematics

- [Task](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/972c82b7f56f18823e4f99aef26e30facd58efcd/Challenge%20Week/Technical%20activity%20deliverable/Task/Assignment_Forward_Kinematics.txt) description
- [Figure](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/972c82b7f56f18823e4f99aef26e30facd58efcd/Challenge%20Week/Technical%20activity%20deliverable/Task/Figure1.png) to verify results
- [Data](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/972c82b7f56f18823e4f99aef26e30facd58efcd/Challenge%20Week/Technical%20activity%20deliverable/Task/jointVariations.mat) to use in the assignment

##### Inverse kinematics

- [Task](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/972c82b7f56f18823e4f99aef26e30facd58efcd/Challenge%20Week/Technical%20activity%20deliverable/Task/Assignment_Inverse_Kinematics.txt) description 
- [Figure](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/972c82b7f56f18823e4f99aef26e30facd58efcd/Challenge%20Week/Technical%20activity%20deliverable/Task/Figure2.png) to verify results
- [Data](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/972c82b7f56f18823e4f99aef26e30facd58efcd/Challenge%20Week/Technical%20activity%20deliverable/Task/endeffectorPosition.mat) to use in the assignment

