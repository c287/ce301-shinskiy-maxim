close all;
disp("forward kinematics")

eep = [];
for jv = 1:length(jointVariations) 
    jointVariations(jv,2)
    jointVariations(jv,3)
    coordinates = forward(jointVariations(jv,2), jointVariations(jv, 3))
    limb1 = coordinates(1:2, :);
    limb2 = coordinates(2:3, :);
    eep = [eep; [limb2(2,1) limb2(2,2)]];
    
    plot(limb1(:,1), limb1(:,2), 'bo-')
    hold on;
    plot(limb2(:,1), limb2(:,2), 'bo-')
    hold on;
    plot(eep(:,1), eep(:,2), 'r*-')
    hold off;
    
    axis([-10 10 -10 10]);
    pause(0.01);
end
    