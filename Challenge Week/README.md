### Challenge Week

[Background Literature](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Challenge%20Week/Background%20Literature) has all literature that was studied during summer preparation.

[Technical Activity deliverable](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/tree/master/Challenge%20Week/Technical%20activity%20deliverable) has a complete solution to the challenge that was set to complete during Challenge Week.

[Presentation](https://cseegit.essex.ac.uk/ce301_2020/ce301_shinskiy_maxim/-/blob/45476a9a24a6a42c23c73ebd9fa4bcc8c71664f9/Challenge%20Week/Challenge_Week_Presentation.pptx) was shown on the meeting with supervisor in the end of the Challenge Week to present the work that was done at that time.
